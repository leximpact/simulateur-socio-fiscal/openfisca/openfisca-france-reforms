# Publish to conda

There is two way of publishing to conda:
- A fully automatic in CI that publish to an "openfisca" channel. See below for more information.
- A more complex for Conda-Forge.

We use both for openfisca-core but only _leximpact channel_ for this project.

## Automatic upload

The CI could automaticaly upload the PyPi and Anaconda package, see the `.gitlab-ci.yml`.

## Manual actions made to make it works the first time

- Create an account on https://anaconda.org.
- Create a token on https://anaconda.org/openfisca/settings/access with _Allow write access to the API site_. Warning, it expire on 2023/01/13.
- Put the token in a CI env variable ANACONDA_TOKEN.

## Manual actions before CI exist

To create the package you can do the following in the project root folder:
```
docker run -i -t -v $PWD:/src continuumio/miniconda3 /bin/bash
cd /src
python3 gitlab-ci/src/get_pypi_info.py -p openfisca-france-reforms
conda install -y conda-build anaconda-client
conda config --set anaconda_upload yes
conda build -c conda-forge --token $ANACONDA_TOKEN --user Leximpact .conda
```
