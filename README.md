# OpenFisca France Reforms

[![PyPI version](https://badge.fury.io/py/openfisca-france-reforms.svg)](https://badge.fury.io/py/openfisca-france-reforms)

[![Conda Version](https://anaconda.org/leximpact/openfisca-france-reforms/badges/version.svg)](https://anaconda.org/Leximpact/openfisca-france-reforms)  ![](https://anaconda.org/leximpact/openfisca-france-reforms/badges/latest_release_date.svg)

## _Some reforms for French OpenFisca tax-benefit system_

## Install

```sh
poetry install
```

## Test Reforms

```sh
# poetry run python -m openfisca_france_reforms.plf_2022.scripts.run_test
# poetry run python -m openfisca_france_reforms.prime_partage_valeur.scripts.run_test
# poetry run python -m openfisca_france_reforms.plf_plfss_2023.scripts.run_test
```