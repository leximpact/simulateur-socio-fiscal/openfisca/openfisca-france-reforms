from openfisca_france import FranceTaxBenefitSystem
from openfisca_france_reforms.plf_plfss_2025 import PlfPlfss2025
from openfisca_france.scenarios import init_single_entity

baseline_tax_benefit_system = FranceTaxBenefitSystem()
baseline_parameters = baseline_tax_benefit_system.parameters
baseline_scenario = baseline_tax_benefit_system.new_scenario()

reform_tax_benefit_system = PlfPlfss2025(baseline_tax_benefit_system)
reform_parameters = reform_tax_benefit_system.parameters

def test_bw_baseline():
    baseline_tax_benefit_system = FranceTaxBenefitSystem()
    baseline_scenario = baseline_tax_benefit_system.new_scenario()
    init_single_entity(baseline_scenario, period='2024-01',  # wide: we simulate for the year
        parent1=dict(salaire_de_base={'2024-01': 21203/12},
        effectif_entreprise=50,
        code_postal_entreprise='75001',
        categorie_salarie='prive_non_cadre',
        )
        )
    baseline_simulation = baseline_scenario.new_simulation()
    print(baseline_simulation.calculate('allegement_general', '2024-01'))
def test_bw_reform():
    reform_tax_benefit_system = PlfPlfss2025(baseline_tax_benefit_system)
    reform_scenario = reform_tax_benefit_system.new_scenario()
    init_single_entity(reform_scenario, period='2024-01',  # wide: we simulate for the year
        parent1=dict(salaire_de_base={'2024-01': 21203/12},
        effectif_entreprise=50,
        code_postal_entreprise='75001',
        categorie_salarie='prive_non_cadre',
        )
        )
    reform_simulation = reform_scenario.new_simulation()
    print(reform_simulation.calculate('allegement_general', '2024-01'))
    
test_bw_baseline()
test_bw_reform()
