from openfisca_core.reforms import Reform
from openfisca_core.parameters import ParameterNode

from ..contrefactuel_plf import modify_parameters as modify_parameters_contrefacturel
from ..inflaters import inflate_parameters

from openfisca_core.model_api import *
from openfisca_france.model.base import *

import os
reform_date = "2025-01-01"


def modify_parameters(parameters):
    parameters = modify_parameters_contrefacturel(parameters)

    inflateurs = ParameterNode(
        "inflateurs",
        directory_path=os.path.join(os.path.dirname(__file__), "parameters/inflateurs/"),
    )

    parameters.children['inflateurs'].children['inflateur_impot'] = inflateurs.inflateur_impot

    param_ir = parameters.impot_revenu
    liste_param_impot = [
        ## revalo sans texte de loi mais usuelle :
        param_ir.bareme_ir_depuis_1945.bareme,
        param_ir.calcul_impot_revenu.plaf_qf.plafond_avantages_procures_par_demi_part,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_couple,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_celib,
        param_ir.calcul_revenus_imposables.abat_rni.enfant_marie,
        param_ir.calcul_revenus_imposables.charges_deductibles.pensions_alimentaires.plafond,
        ## revalo selon 1e tranche de l'IR
        param_ir.calcul_revenus_imposables.deductions.abatpen,
        param_ir.calcul_revenus_imposables.deductions.abatpro,
        param_ir.calcul_revenus_imposables.charges_deductibles.accueil_personne_agee.plafond,
        param_ir.calcul_revenus_imposables.abat_rni.contribuable_age_invalide,
    ]
    for noeud in liste_param_impot:
        inflate_parameters(
            noeud,
            inflator=parameters(reform_date).inflateurs.inflateur_impot,
            base_year=2023,
            last_year=2024,
            start_instant=f"{2024}-01-01",
            round_ndigits=0,
        )
    # # En attendant les véritables PLF et PLFSS 2025,
    # # - applique une augmentation de 1 € de chaque seuil du barème de l'IR
    # # - applique une augmentation de 1 % du taux de la décote

    # impot_revenu = parameters.impot_revenu
    # bareme = impot_revenu.bareme_ir_depuis_1945.bareme
    # for bracket in bareme.brackets:
    #     threshold = bracket.threshold
    #     threshold_value = threshold.get_at_instant(reform_date)
    #     if threshold_value is not None:
    #         threshold.update(start=reform_date, value=round(threshold_value * 1.048, 2))

    # plaf_qf = impot_revenu.calcul_impot_revenu.plaf_qf
    # decote = plaf_qf.decote

    param_ce = ParameterNode(
        "contribution_exceptionnelle",
        directory_path=os.path.join(os.path.dirname(__file__), "parameters/contribution_exceptionnelle/"),
    )
    parameters.add_child("contribution_exceptionnelle", param_ce)
    param_pfu = ParameterNode(
        "pfu",
        directory_path=os.path.join(os.path.dirname(__file__), "parameters/pfu/"),
    )
    parameters.taxation_capital.prelevement_forfaitaire.partir_2018.add_child("pfu", param_pfu)

    return parameters


# Cette classe n'hérite pas de ContrefactuelPlf, car actuellement, les méthodes d'une
# réforme ne permettent pas l'héritage entre 2 réformes (La méthode modify_parameters
# n'est pas composable).
# Il faut donc reproduire ici le comportement de la méthode ContrefactuelPlf.apply.
class PlfPlfss2025(Reform):
    name = "PLF & PLFSS 2025"
    tax_benefit_system_name = "openfisca_france_with_indirect_taxation"

    def apply(self):

        class contribution_exceptionnelle(Variable):
            value_type = float
            entity = FoyerFiscal
            label = 'Contribution exceptionnelle pour tout le monde en 2025'
            reference = 'http://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069577&idSectionTA=LEGISCTA000025049019'
            definition_period = YEAR

            def formula_2025_01_01(foyer_fiscal, period, parameters):
                rfr = foyer_fiscal('rfr', period)
                bareme = parameters(period).contribution_exceptionnelle.bareme

                return -bareme.calc(rfr)

        class contribution_exceptionnelle_taux(Variable):
            value_type = float
            entity = FoyerFiscal
            label = 'Contribution exceptionnelle pour tout le monde en 2025'
            reference = 'http://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069577&idSectionTA=LEGISCTA000025049019'
            definition_period = YEAR

            def formula_2025_01_01(foyer_fiscal, period, parameters):
                rfr = foyer_fiscal('rfr', period)
                taux = parameters(period).contribution_exceptionnelle.taux

                return -rfr * taux


        class impots_directs(Variable):
            value_type = float
            entity = Menage
            label = 'Impôts directs'
            reference = 'http://fr.wikipedia.org/wiki/Imp%C3%B4t_direct'
            definition_period = YEAR

            def formula_2025_01_01(menage, period, parameters):
                '''
                Pour les impôts définis au niveau du foyer fiscal :
                on prend en compte l'impôt des foyers fiscaux dont le déclarant principal est dans le ménage
                '''
                taxe_habitation = menage('taxe_habitation', period)

                irpp_economique_i = menage.members.foyer_fiscal('irpp_economique', period)
                irpp_economique = menage.sum(irpp_economique_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On comptabilise ir_pv_immo ici directement, et non pas dans la variable 'impot_revenu_restant_a_payer', car administrativement, cet impôt n'est pas dans l'impot_revenu_restant_a_payer, et n'est déclaré dans le formulaire 2042C que pour calculer le revenu fiscal de référence. On colle à la définition administrative, afin d'avoir une variable 'impot_revenu_restant_a_payer' qui soit comparable à l'IR du simulateur en ligne de la DGFiP
                ir_pv_immo_i = menage.members.foyer_fiscal('ir_pv_immo', period)
                ir_pv_immo = menage.sum(ir_pv_immo_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                isf_ifi_i = menage.members.foyer_fiscal('isf_ifi', period)
                isf_ifi = menage.sum(isf_ifi_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                prelevement_liberatoire_autoentrepreneur_i = menage.members.foyer_fiscal('microsocial', period)
                prelevement_liberatoire_autoentrepreneur = menage.sum(prelevement_liberatoire_autoentrepreneur_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                contribution_exceptionnelle_i = menage.members.foyer_fiscal('contribution_exceptionnelle', period)
                contribution_exceptionnelle = menage.sum(contribution_exceptionnelle_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                contribution_exceptionnelle_taux_i = menage.members.foyer_fiscal('contribution_exceptionnelle_taux', period)
                contribution_exceptionnelle_taux = menage.sum(contribution_exceptionnelle_taux_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return (
                    taxe_habitation
                    + irpp_economique
                    + ir_pv_immo
                    + isf_ifi
                    + prelevement_liberatoire_autoentrepreneur
                    + contribution_exceptionnelle
                    + contribution_exceptionnelle_taux
                )

            def formula(menage, period, parameters):
                '''
                Pour les impôts définis au niveau du foyer fiscal :
                on prend en compte l'impôt des foyers fiscaux dont le déclarant principal est dans le ménage
                '''
                taxe_habitation = menage('taxe_habitation', period)

                irpp_economique_i = menage.members.foyer_fiscal('irpp_economique', period)
                irpp_economique = menage.sum(irpp_economique_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On comptabilise ir_pv_immo ici directement, et non pas dans la variable 'impot_revenu_restant_a_payer', car administrativement, cet impôt n'est pas dans l'impot_revenu_restant_a_payer, et n'est déclaré dans le formulaire 2042C que pour calculer le revenu fiscal de référence. On colle à la définition administrative, afin d'avoir une variable 'impot_revenu_restant_a_payer' qui soit comparable à l'IR du simulateur en ligne de la DGFiP
                ir_pv_immo_i = menage.members.foyer_fiscal('ir_pv_immo', period)
                ir_pv_immo = menage.sum(ir_pv_immo_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                isf_ifi_i = menage.members.foyer_fiscal('isf_ifi', period)
                isf_ifi = menage.sum(isf_ifi_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                prelevement_liberatoire_autoentrepreneur_i = menage.members.foyer_fiscal('microsocial', period)
                prelevement_liberatoire_autoentrepreneur = menage.sum(prelevement_liberatoire_autoentrepreneur_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return (
                    taxe_habitation
                    + irpp_economique
                    + ir_pv_immo
                    + isf_ifi
                    + prelevement_liberatoire_autoentrepreneur
                )


        class prelevement_forfaitaire_unique_ir_hors_assurance_vie(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Partie du prélèvement forfaitaire unique associée à l'impôt sur le revenu (hors assurance-vie, épargne solidaire et produits venant des états non-coopératifs)"
            reference = [
                'Article 28 de la Loi n° 2017-1837 du 30 décembre 2017 de finances pour 2018 (modifie art. 125 A, 125-0 A, 200 A et art. 117 quater du CGI)',
                'https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000036377422&cidTexte=JORFTEXT000036339197'
                ]
            definition_period = YEAR

            def formula_2025_01_01(foyer_fiscal, period, parameters):
                P = parameters(period).taxation_capital.prelevement_forfaitaire.partir_2018.pfu

                # Revenus des valeurs et capitaux mobiliers hors assurance-vie
                revenus_capitaux_prelevement_forfaitaire_unique_ir = foyer_fiscal('revenus_capitaux_prelevement_forfaitaire_unique_ir', period, options = [ADD])
                rfr = foyer_fiscal('rfr', period)
                assurance_vie_pfu_ir = foyer_fiscal('assurance_vie_pfu_ir', period)
                revenus_capitaux_prelevement_forfaitaire_unique_ir_hors_assurance_vie = (
                    revenus_capitaux_prelevement_forfaitaire_unique_ir
                    - assurance_vie_pfu_ir
                    )

                # Plus-values
                plus_values_prelevement_forfaitaire_unique_ir = foyer_fiscal('plus_values_prelevement_forfaitaire_unique_ir', period)

                assiette_pfu_hors_assurance_vie = (
                    revenus_capitaux_prelevement_forfaitaire_unique_ir_hors_assurance_vie
                    + plus_values_prelevement_forfaitaire_unique_ir
                    ) + P.taux_rfr * rfr

                return -assiette_pfu_hors_assurance_vie * P.taux

            def formula_2018_01_01(foyer_fiscal, period, parameters):
                P = parameters(period).taxation_capital.prelevement_forfaitaire.partir_2018

                # Revenus des valeurs et capitaux mobiliers hors assurance-vie
                revenus_capitaux_prelevement_forfaitaire_unique_ir = foyer_fiscal('revenus_capitaux_prelevement_forfaitaire_unique_ir', period, options = [ADD])
                assurance_vie_pfu_ir = foyer_fiscal('assurance_vie_pfu_ir', period)
                revenus_capitaux_prelevement_forfaitaire_unique_ir_hors_assurance_vie = (
                    revenus_capitaux_prelevement_forfaitaire_unique_ir
                    - assurance_vie_pfu_ir
                    )

                # Plus-values
                plus_values_prelevement_forfaitaire_unique_ir = foyer_fiscal('plus_values_prelevement_forfaitaire_unique_ir', period)

                assiette_pfu_hors_assurance_vie = (
                    revenus_capitaux_prelevement_forfaitaire_unique_ir_hors_assurance_vie
                    + plus_values_prelevement_forfaitaire_unique_ir
                    )

                return -assiette_pfu_hors_assurance_vie * P.taux_prelevement_forfaitaire_rev_capital_eligibles_pfu_interets_dividendes_etc

        self.modify_parameters(modifier_function=modify_parameters)
        self.add_variable(contribution_exceptionnelle)
        self.add_variable(contribution_exceptionnelle_taux)
        
        for variable in [prelevement_forfaitaire_unique_ir_hors_assurance_vie, impots_directs]:
            self.update_variable(variable)