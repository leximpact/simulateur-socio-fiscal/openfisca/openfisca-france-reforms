from openfisca_core.parameters import ParameterNode


def modify_metadata(parameters):
    param_ir = parameters.impot_revenu

    add_metadata(
        param_ir.calcul_revenus_imposables.deductions.abatpen.min,
        {
            "inflator_reference": {
                "title": "Article 158-5-a. du Code général des impôts",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000047622551",
            }
        },
    )

    add_metadata(
        param_ir.calcul_revenus_imposables.deductions.abatpen.max,
        {
            "inflator_reference": {
                "title": "Article 158-5-a. du Code général des impôts",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000047622551",
            }
        },
    )

    add_metadata(
        param_ir.calcul_revenus_imposables.deductions.abatpro.min,
        {
            "inflator_reference": {
                "title": "Article 83, 3. du Code général des impôts",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000047622408",
            }
        },
    )

    add_metadata(
        param_ir.calcul_revenus_imposables.deductions.abatpro.max,
        {
            "inflator_reference": {
                "title": "Article 83, 3. du Code général des impôts",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000047622408",
            }
        },
    )

    add_metadata(
        param_ir.calcul_revenus_imposables.charges_deductibles.accueil_personne_agee.plafond,
        {
            "inflator_reference": {
                "title": "Article 156 II-2° ter, du Code général des impôts",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046673952",
            }
        },
    )

    add_metadata(
        param_ir.calcul_revenus_imposables.abat_rni.contribuable_age_invalide,
        {
            "inflator_reference": {
                "title": "Article 157 bis du Code général des impôts",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042158963",
            }
        },
    )

    add_metadata(
        param_ir.calcul_reductions_impots.dons.dons_coluche.plafond,
        {
            "inflator_reference": {
                "title": "Article 200, 1 ter. du Code général des impôts",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000045764695",
            }
        },
    )

    add_metadata(
        parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils,
        {
            "inflator_reference": {
                "title": "Article L136-8 III ter, du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042683546",
            }
        },
    )

    add_metadata(
        parameters.prelevements_sociaux.pss,
        {
            "inflator_reference": {
                "title": "Article D242-17 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033516182",
            }
        },
    )

    param_apl = parameters.prestations_sociales.aides_logement.allocations_logement
    add_metadata(
        param_apl.al_loc2.par_zone,
        {
            "inflator_reference": {
                "title": "Article L823-4 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046194144",
            }
        },
    )

    add_metadata(
        param_apl.al_charge,
        {
            "inflator_reference": {
                "title": "Article L823-4 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046194144",
            }
        },
    )

    add_metadata(
        param_apl.al_plaf_logement_foyer,
        {
            "inflator_reference": {
                "title": "Article L823-4 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046194144",
            }
        },
    )

    add_metadata(
        param_apl.al_etudiant,
        {
            "inflator_reference": {
                "title": "Article L823-4 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046194144",
            }
        },
    )

    add_metadata(
        param_apl.al_loc2.montant_forfaitaire_participation_minimale_po,
        {
            "inflator_reference": {
                "title": "Article L823-4 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000046194144",
            }
        },
    )

    add_metadata(
        param_apl.al_param_r0.r0,
        {
            "inflator_reference": {
                "title": "Article D823-17 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041419255",
            }
        },
    )

    param_apl_etudiant = parameters.prestations_sociales.aides_logement.allocations_logement
    add_metadata(
        param_apl_etudiant.ressources.dar_4,
        {
            "inflator_reference": {
                "title": "Article D922-21 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041419255",
            }
        },
    )

    add_metadata(
        param_apl_etudiant.ressources.dar_5,
        {
            "inflator_reference": {
                "title": "Article D922-21 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041419255",
            }
        },
    )

    add_metadata(
        param_apl_etudiant.ressources.dar_11,
        {
            "inflator_reference": {
                "title": "Article D922-21 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041419255",
            }
        },
    )

    add_metadata(
        param_apl_etudiant.ressources.dar_12,
        {
            "inflator_reference": {
                "title": "Article D922-21 du code de la construction et de l'habitation",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041419255",
            }
        },
    )

    param_prestations = parameters.prestations_sociales
    add_metadata(
        param_prestations.solidarite_insertion.minimum_vieillesse.aspa.plafond_ressources,
        {
            "inflator_reference": {
                "title": "Article L816-2 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000036393188",
            }
        },
    )

    add_metadata(
        param_prestations.solidarite_insertion.minimum_vieillesse.aspa.montant_maximum_annuel,
        {
            "inflator_reference": {
                "title": "Article L816-2 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000036393188",
            }
        },
    )

    add_metadata(
        param_prestations.solidarite_insertion.minima_sociaux.rsa.rsa_m.montant_de_base_du_rsa,
        {
            "inflator_reference": {
                "title": "Article L262-3 du code de l'action sociale et des familles",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038833721",
            }
        },
    )

    add_metadata(
        param_prestations.solidarite_insertion.minima_sociaux.ppa.pa_m.montant_de_base,
        {
            "inflator_reference": {
                "title": "Article L842-3 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000037994814",
            }
        },
    )

    add_metadata(
        param_prestations.prestations_etat_de_sante.invalidite.asi.plafond_ressource_couple,
        {
            "inflator_reference": {
                "title": "Article L816-3 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041474264",
            }
        },
    )

    add_metadata(
        param_prestations.prestations_etat_de_sante.invalidite.asi.plafond_ressource_seul,
        {
            "inflator_reference": {
                "title": "Article L816-3 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041474264",
            }
        },
    )

    add_metadata(
        param_prestations.prestations_etat_de_sante.invalidite.aah.montant,
        {
            "inflator_reference": {
                "title": "Article L821-3-1 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031781089",
            }
        },
    )

    add_metadata(
        param_prestations.prestations_familiales.bmaf.bmaf,
        {
            "inflator_reference": {
                "title": "Article 5131-19 du Code du travail",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031688371",
            }
        },
    )

    add_metadata(
        param_prestations.education.contrat_engagement_jeune.montants,
        {
            "inflator_reference": {
                "title": "Article L551-1 du code de la sécurité sociale",
                "href": "https://www.legifrance.gouv.fr/codes/id/LEGIARTI000045207447/2023-07-06/#LEGIARTI000045207447",
            }
        },
    )

    add_metadata(
        parameters.chomage.allocations_assurance_chomage.ass.montant_plein,
        {
            "inflator_reference": {
                "title": "Article L5423-6 du code du travail",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031781118",
            }
        },
    )

    add_metadata(
        parameters.chomage.allocations_assurance_chomage.ass.montant_plein_mayotte,
        {
            "inflator_reference": {
                "title": "Article L5423-6 du code du travail",
                "href": "https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031781118",
            }
        },
    )

    return parameters


def add_metadata(parameters, metadata: dict):
    if isinstance(parameters, ParameterNode):
        for sub_parameter in parameters.children.values():
            add_metadata(sub_parameter, metadata)
    else:
        parameters.metadata.update(metadata)
