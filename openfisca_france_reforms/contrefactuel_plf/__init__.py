import os
from openfisca_france_reforms.contrefactuel_plf.inflator_references import (
    modify_metadata,
    add_metadata,
)

from openfisca_core.parameters import ParameterNode
from openfisca_core.reforms import Reform

from ..inflaters import inflate_parameters


year_contrefactuel = 2025


def modify_parameters(parameters):
    inflateurs_parameters = ParameterNode(
        "inflateurs",
        directory_path=os.path.join(os.path.dirname(__file__), "parameters/inflateurs"),
    )

    parameters.add_child("inflateurs", inflateurs_parameters)

    ## IR
    param_ir = parameters.impot_revenu
    liste_param_impot = [
        ## revalo sans texte de loi mais usuelle :
        param_ir.bareme_ir_depuis_1945.bareme,
        param_ir.calcul_impot_revenu.plaf_qf.plafond_avantages_procures_par_demi_part,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_couple,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_celib,
        param_ir.calcul_revenus_imposables.abat_rni.enfant_marie,
        param_ir.calcul_revenus_imposables.charges_deductibles.pensions_alimentaires.plafond,
        ## revalo selon 1e tranche de l'IR
        param_ir.calcul_revenus_imposables.deductions.abatpen.min,
        param_ir.calcul_revenus_imposables.deductions.abatpen.max,
        param_ir.calcul_revenus_imposables.deductions.abatpro.min,
        param_ir.calcul_revenus_imposables.deductions.abatpro.max,
        param_ir.calcul_revenus_imposables.charges_deductibles.accueil_personne_agee.plafond,
        param_ir.calcul_revenus_imposables.abat_rni.contribuable_age_invalide,
        ## De base revalorisation selon 1e tranche de l'IR mais revalo exceptionnelle fixée pour 2020 à 2023 :
        # param_ir.calcul_reductions_impots.dons.dons_coluche.plafond,
        ## Evolution triennale selon 1e tranche bareme, derniere revalo revenus 2022
        # param_ir.calcul_revenus_imposables.rpns.micro.microentreprise.regime_micro_bnc.marchandises.plafond,
        # param_ir.calcul_revenus_imposables.rpns.micro.microentreprise.regime_micro_ba.plafond_recettes,
        # param_ir.calcul_revenus_imposables.rpns.micro.microentreprise.regime_micro_bnc.services/plafond,
    ]
    for noeud in liste_param_impot:
        inflate_parameters(
            noeud,
            inflator=inflateurs_parameters(year_contrefactuel).inflateur_impot,
            base_year=year_contrefactuel - 2,
            last_year=year_contrefactuel - 1,
            start_instant=f"{year_contrefactuel - 1}-01-01",
            round_ndigits=0,
        )
        add_metadata(noeud, {"inflator": "inflateurs/inflateur_impot"})

    ## CSG
    ### On commente de manière temporaire pour gérer le budget début 2025
    # inflate_parameters(
    #     parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils,
    #     inflator=inflateurs_parameters(year_contrefactuel).inflateur_csg,
    #     base_year=year_contrefactuel - 1,
    #     last_year=year_contrefactuel,
    #     start_instant=f"{year_contrefactuel}-01-01",
    #     round_ndigits=0,
    # )
    # add_metadata(
    #     parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils,
    #     {"inflator": "inflateurs/inflateur_csg"},
    # )

    ## PSS (utilisé pour le calcul des cotisations sociales)
    ### On commente de manière temporaire pour gérer le budget début 2025
    # inflate_parameters(
    #     parameters.prelevements_sociaux.pss,
    #     inflator=inflateurs_parameters(year_contrefactuel).inflateur_pss,
    #     base_year=year_contrefactuel - 1,
    #     last_year=year_contrefactuel,
    #     start_instant=f"{year_contrefactuel}-01-01",
    #     round_ndigits=0,
    # )
    # add_metadata(
    #     parameters.prelevements_sociaux.pss, {"inflator": "inflateurs/inflateur_pss"}
    # )

    ## Aides au logement :

    param_apl = parameters.prestations_sociales.aides_logement.allocations_logement
    ### aides au logement paramètres loyers
    list_param_al_loyer = [
        param_apl.al_loc2.par_zone,
        param_apl.al_charge,
        param_apl.al_plaf_logement_foyer,
        param_apl.al_etudiant,
        param_apl.al_loc2.montant_forfaitaire_participation_minimale_po,
    ]
    for param in list_param_al_loyer:
        inflate_parameters(
            param,
            inflator=inflateurs_parameters(
                year_contrefactuel - 1
            ).inflateur_aides_logement_loyer,
            base_year=year_contrefactuel - 2,
            last_year=year_contrefactuel - 1,
            start_instant=f"{year_contrefactuel - 1}-10-01",
            round_ndigits=0,
            ignore_missing_units=True,
        )
        add_metadata(
            param, {"inflator": "inflateurs/inflateur_aides_logement_loyer"}
        )

        ## TODO : rajouter une option pour l'inflation en octobre N-1 tant que décret pour octobre n'est pas sorti

    ### parametres de ressources

    # inflate_parameters(
    #     param_apl.al_param_r0.r0,
    #     inflator=inflateurs_parameters(
    #         year_contrefactuel
    #     ).inflateur_aides_logement_depenses,
    #     base_year=year_contrefactuel - 1,
    #     last_year=year_contrefactuel,
    #     start_instant=f"{year_contrefactuel}-01-01",
    #     round_ndigits=0,
    # )
    # add_metadata(
    #     param_apl.al_param_r0.r0,
    #     {"inflator": "inflateurs/inflateur_aides_logement_depenses"},
    # )

    ### attention il y a un sous dossier mayotte qui ne rentre peut être pas dans ce qui devrait être revalorisé. Mais pour l'instant on n'a pas le détail Mayotte
    # param_apl_etudiant = parameters.prestations_sociales.aides_logement.allocations_logement
    # ### forfait étudiant
    # list_param_forfait_etudiant = [
    #     param_apl_etudiant.ressources.dar_4,
    #     param_apl_etudiant.ressources.dar_5,
    #     param_apl_etudiant.ressources.dar_11,
    #     param_apl_etudiant.ressources.dar_12,
    # ]
    # for param in list_param_forfait_etudiant:
    #     inflate_parameters(
    #         param,
    #         inflator=inflateurs_parameters(
    #             year_contrefactuel
    #         ).inflateur_aides_logement_forfait_etudiant,
    #         base_year=year_contrefactuel - 1,
    #         last_year=year_contrefactuel,
    #         start_instant=f"{year_contrefactuel}-01-01",
    #         round_ndigits=0,
    #     )
    #     add_metadata(
    #         param,
    #         {"inflator": "inflateurs/inflateur_aides_logement_forfait_etudiant"},
    #     )

    ## Prestations sociales revalorisees en janvier

    param_prestations = parameters.prestations_sociales

    ### On commente de manière temporaire pour gérer le budget début 2025
    # list_param_prestats_janvier = [
    #     param_prestations.solidarite_insertion.minimum_vieillesse.aspa.plafond_ressources,
    #     param_prestations.solidarite_insertion.minimum_vieillesse.aspa.montant_maximum_annuel,
    # ]

    # for param in list_param_prestats_janvier:
    #     inflate_parameters(
    #         param,
    #         inflator=inflateurs_parameters(
    #             year_contrefactuel
    #         ).inflateur_prestations_janvier,
    #         base_year=year_contrefactuel - 1,
    #         last_year=year_contrefactuel,
    #         start_instant=f"{year_contrefactuel}-01-01",
    #         round_ndigits=2,
    #     )
    #     add_metadata(param, {"inflator": "inflateurs/inflateur_prestations_janvier"})

    list_param_prestats_avril = [
        param_prestations.solidarite_insertion.minima_sociaux.rsa.rsa_m.montant_de_base_du_rsa,
        param_prestations.solidarite_insertion.minima_sociaux.ppa.pa_m.montant_de_base,
        param_prestations.prestations_etat_de_sante.invalidite.asi.plafond_ressource_couple,
        param_prestations.prestations_etat_de_sante.invalidite.asi.plafond_ressource_seul,
        param_prestations.prestations_etat_de_sante.invalidite.aah.montant,
        param_prestations.prestations_familiales.bmaf.bmaf,
        param_prestations.education.contrat_engagement_jeune.montants,
    ]

    for param in list_param_prestats_avril:
        inflate_parameters(
            param,
            inflator=inflateurs_parameters(
                year_contrefactuel
            ).inflateur_prestations_avril,
            base_year=year_contrefactuel - 1,
            last_year=year_contrefactuel,
            start_instant=f"{year_contrefactuel}-04-01",
            round_ndigits=2,
        )
        add_metadata(param, {"inflator": "inflateurs/inflateur_prestations_avril"})

    modify_metadata(parameters)

    return parameters


class ContrefactuelPlf(Reform):
    name = "Contrefactuel PLF"
    tax_benefit_system_name = "openfisca_france_with_indirect_taxation"

    def apply(self):
        self.modify_parameters(modifier_function=modify_parameters)
