from openfisca_core.reforms import Reform

from ..contrefactuel_plf import modify_parameters as modify_parameters_contrefacturel
from ..inflaters import inflate_parameters

reform_date = "2024-01-01"


def modify_parameters(parameters):
    parameters = modify_parameters_contrefacturel(parameters)
    param_ir = parameters.impot_revenu
    liste_param_impot = [
        ## revalo sans texte de loi mais usuelle :
        param_ir.bareme_ir_depuis_1945.bareme,
        param_ir.calcul_impot_revenu.plaf_qf.plafond_avantages_procures_par_demi_part,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_couple,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_celib,
        param_ir.calcul_revenus_imposables.abat_rni.enfant_marie,
        param_ir.calcul_revenus_imposables.charges_deductibles.pensions_alimentaires.plafond,
        ## revalo selon 1e tranche de l'IR
        param_ir.calcul_revenus_imposables.deductions.abatpen,
        param_ir.calcul_revenus_imposables.deductions.abatpro,
        param_ir.calcul_revenus_imposables.charges_deductibles.accueil_personne_agee.plafond,
        param_ir.calcul_revenus_imposables.abat_rni.contribuable_age_invalide,
    ]
    for noeud in liste_param_impot:
        inflate_parameters(
            noeud,
            inflator=0.048,
            base_year=2022,
            last_year=2023,
            start_instant=f"{2023}-01-01",
            round_ndigits=0,
        )
    # # En attendant les véritables PLF et PLFSS 2024,
    # # - applique une augmentation de 1 € de chaque seuil du barème de l'IR
    # # - applique une augmentation de 1 % du taux de la décote

    # impot_revenu = parameters.impot_revenu
    # bareme = impot_revenu.bareme_ir_depuis_1945.bareme
    # for bracket in bareme.brackets:
    #     threshold = bracket.threshold
    #     threshold_value = threshold.get_at_instant(reform_date)
    #     if threshold_value is not None:
    #         threshold.update(start=reform_date, value=round(threshold_value * 1.048, 2))

    # plaf_qf = impot_revenu.calcul_impot_revenu.plaf_qf
    # decote = plaf_qf.decote

    return parameters


# Cette classe n'hérite pas de ContrefactuelPlf, car actuellement, les méthodes d'une
# réforme ne permettent pas l'héritage entre 2 réformes (La méthode modify_parameters
# n'est pas composable).
# Il faut donc reproduire ici le comportement de la méthode ContrefactuelPlf.apply.
class PlfPlfss2024(Reform):
    name = "PLF & PLFSS 2024"
    tax_benefit_system_name = "openfisca_france_with_indirect_taxation"

    def apply(self):
        self.modify_parameters(modifier_function=modify_parameters)
