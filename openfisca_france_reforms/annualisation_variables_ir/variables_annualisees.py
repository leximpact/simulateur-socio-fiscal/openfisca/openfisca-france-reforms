from openfisca_france.model.base import (
    FoyerFiscal,
    Individu,
    YEAR,
    Variable,
    Reform,
)


class AnnualisationVariablesIR(Reform):
    name = "Annualisation variables calcul de l'impôt sur le revenu"
    tax_benefit_system_name = "openfisca_france_with_indirect_taxation"

    def apply(self):

        class salaire_imposable(Variable):
            value_type = float
            unit = 'currency'
            cerfa_field = {  # (f1aj, f1bj, f1cj, f1dj, f1ej)
                0: '1AJ',
                1: '1BJ',
                2: '1CJ',
                3: '1DJ',
                4: '1EJ',
                }
            entity = Individu
            label = 'Salaires imposables'
            reference = 'https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042683657'
            definition_period = YEAR

        class retraite_imposable(Variable):
            unit = 'currency'
            value_type = float
            cerfa_field = {
                0: '1AS',
                1: '1BS',
                2: '1CS',
                3: '1DS',
                4: '1ES',
                }
            entity = Individu
            label = 'Retraites au sens strict imposables (rentes à titre onéreux exclues)'
            reference = 'http://vosdroits.service-public.fr/particuliers/F415.xhtml'
            definition_period = YEAR

        class chomage_imposable(Variable):
            value_type = float
            unit = 'currency'
            cerfa_field = {
                0: '1AP',
                1: '1BP',
                2: '1CP',
                3: '1DP',
                4: '1EP',
                }
            entity = Individu
            label = 'Allocations chômage imposables'
            reference = 'http://www.insee.fr/fr/methodes/default.asp?page=definitions/chomage.htm'
            definition_period = YEAR

        baseline_revenus_capitaux_pfu = self.baseline.get_variable(
            "revenus_capitaux_prelevement_forfaitaire_unique_ir"
        )
        formula_revenus_capitaux_pfu = (
            baseline_revenus_capitaux_pfu.get_formula("2021-01-01")
        )

        class revenus_capitaux_prelevement_forfaitaire_unique_ir(Variable):
            value_type = float
            entity = FoyerFiscal
            label = 'Revenus des valeurs et capitaux mobiliers soumis au prélèvement forfaitaire unique (partie impôt sur le revenu)'
            definition_period = YEAR

            def formula_2021_01_01(foyer_fiscal, period, parameters):
                return formula_revenus_capitaux_pfu(foyer_fiscal, period.first_month, parameters) * 12
            
        baseline_revenus_capitaux_prelevement_bareme = self.baseline.get_variable(
            "revenus_capitaux_prelevement_bareme"
        )
        formula_revenus_capitaux_prelevement_bareme = (
            baseline_revenus_capitaux_prelevement_bareme.get_formula("2021-01-01")
        )

        class revenus_capitaux_prelevement_bareme(Variable):
            value_type = float
            entity = FoyerFiscal
            label = 'Revenus du capital imposés au barème (montants bruts)'
            reference = 'http://bofip.impots.gouv.fr/bofip/3775-PGP'
            definition_period = YEAR
            
            def formula_2021_01_01(foyer_fiscal, period, parameters):
                return formula_revenus_capitaux_prelevement_bareme(foyer_fiscal, period.first_month, parameters) * 12

        baseline_revenus_capitaux_prelevement_liberatoire = self.baseline.get_variable(
            "revenus_capitaux_prelevement_liberatoire"
        )
        formula_revenus_capitaux_prelevement_liberatoire = (
            baseline_revenus_capitaux_prelevement_liberatoire.get_formula("2021-01-01")
        )

        class revenus_capitaux_prelevement_liberatoire(Variable):
            value_type = float
            entity = FoyerFiscal
            label = 'Revenu du capital imposé au prélèvement libératoire (montants bruts)'
            reference = 'http://bofip.impots.gouv.fr/bofip/3817-PGP'
            definition_period = YEAR

            def formula_2021_01_01(foyer_fiscal, period, parameters):
                return formula_revenus_capitaux_prelevement_liberatoire(foyer_fiscal, period.first_month, parameters) * 12
        
        variables_annualisees = [
            salaire_imposable,
            retraite_imposable,
            chomage_imposable,
            revenus_capitaux_prelevement_forfaitaire_unique_ir,
            revenus_capitaux_prelevement_bareme,
            revenus_capitaux_prelevement_liberatoire
        ]

        for variable in variables_annualisees:
            self.update_variable(variable)