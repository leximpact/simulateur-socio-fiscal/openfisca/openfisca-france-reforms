import logging

from numpy import busday_count, datetime64, logical_or as or_, logical_and as and_, timedelta64

from openfisca_core.periods import Period

from openfisca_france.model.base import *

log = logging.getLogger(__name__)


class allegement_general(Variable):
    value_type = float
    entity = Individu
    label = 'Réduction générale des cotisations patronales (dite réduction Fillon)'
    reference = 'https://www.service-public.fr/professionnels-entreprises/vosdroits/F24542'
    definition_period = MONTH
    calculate_output = calculate_output_add
    set_input = set_input_divide_by_period

    # Attention : cet allègement a des règles de cumul spécifiques

    def formula_2005_07_01(individu, period, parameters):
        stagiaire = individu('stagiaire', period)
        apprenti = individu('apprenti', period)
        allegement_mode_recouvrement = individu('allegement_general_mode_recouvrement', period)
        exoneration_cotisations_employeur_jei = individu('exoneration_cotisations_employeur_jei', period)
        exoneration_cotisations_employeur_tode = individu('exoneration_cotisations_employeur_tode', period)
        non_cumulee = not_(exoneration_cotisations_employeur_jei + exoneration_cotisations_employeur_tode)

        # switch on 3 possible payment options
        allegement = switch_on_allegement_mode(
            individu, period, parameters,
            allegement_mode_recouvrement,
            'allegement_general',
            )

        return allegement * not_(stagiaire) * not_(apprenti) * non_cumulee


def compute_allegement_general(individu, period, parameters):
    '''
        Exonération générale de cotisations patronales
        https://www.service-public.fr/professionnels-entreprises/vosdroits/F24542
    '''

    assiette = individu('assiette_allegement', period)
    smic_proratise = individu('smic_proratise', period)
    effectif_entreprise = individu('effectif_entreprise', period)

    # Calcul du taux
    # Le montant maximum de l’allègement dépend de l’effectif de l’entreprise.
    # Le montant est calculé chaque année civile, pour chaque salarié ;
    # il est égal au produit de la totalité de la rémunération annuelle telle
    # que visée à l’article L. 242-1 du code de la Sécurité sociale par un
    # coefficient.
    # Ce montant est majoré de 10 % pour les entreprises de travail temporaire
    # au titre des salariés temporaires pour lesquels elle est tenue à
    # l’obligation d’indemnisation compensatrice de congés payés.

    allegement_general = parameters(period).prelevements_sociaux.reductions_cotisations_sociales.allegement_general

    # Du 2003-07-01 au 2005-06-30
    if date(2003, 7, 1) <= period.start.date <= date(2005, 6, 30):
        seuil = allegement_general.entreprises_ayant_signe_un_accord_de_rtt_avant_le_30_06_2003.plafond
        tx_max = allegement_general.entreprises_ayant_signe_un_accord_de_rtt_avant_le_30_06_2003.reduction_maximale
    # Du 2005-07-01 au 2019-12-31
    elif date(2005, 7, 1) <= period.start.date <= date(2019, 12, 31):
        seuil = allegement_general.ensemble_des_entreprises.plafond
        petite_entreprise = (effectif_entreprise < 20)
        tx_max = (
            allegement_general.ensemble_des_entreprises.entreprises_de_20_salaries_et_plus
            * not_(petite_entreprise)
            + allegement_general.ensemble_des_entreprises.entreprises_de_moins_de_20_salaries
            * petite_entreprise
            )
    # Après le 2019-12-31
    else:
        seuil = allegement_general.ensemble_des_entreprises.plafond
        petite_entreprise = (effectif_entreprise < 50)
        tx_max = (
            allegement_general.ensemble_des_entreprises.entreprises_de_50_salaries_et_plus
            * not_(petite_entreprise)
            + allegement_general.ensemble_des_entreprises.entreprises_de_moins_de_50_salaries
            * petite_entreprise
            )

    if seuil <= 1:
        return 0
    if period.start.date >= date(2024,10,10):
        prime_partage_valeur_exoneree = individu('prime_partage_valeur_exoneree', period, options=[DIVIDE])
        assiette = assiette + prime_partage_valeur_exoneree

    ratio_smic_salaire = smic_proratise / (assiette + 1e-16)
    # règle d'arrondi: 4 décimales au dix-millième le plus proche
    taux_allegement_general = round_(tx_max * min_(1, max_(seuil * ratio_smic_salaire - 1, 0) / (seuil - 1)), 4)

    # Montant de l'allegment
    return taux_allegement_general * assiette


###############################
#  Helper functions and classes
###############################


def switch_on_allegement_mode(individu, period, parameters, mode_recouvrement, variable_name):
    '''
        Switch on 3 possible payment options for allegements

        Name of the computation method specific to the allegement
        should precisely be the variable name prefixed with 'compute_'
    '''
    compute_function = globals()['compute_' + variable_name]
    TypesAllegementModeRecouvrement = mode_recouvrement.possible_values
    recouvrement_fin_annee = (mode_recouvrement == TypesAllegementModeRecouvrement.fin_d_annee)
    recouvrement_anticipe = (mode_recouvrement == TypesAllegementModeRecouvrement.anticipe)
    recouvrement_progressif = (mode_recouvrement == TypesAllegementModeRecouvrement.progressif)

    return (
        (recouvrement_fin_annee * compute_allegement_annuel(individu, period, parameters, variable_name, compute_function))
        + (recouvrement_anticipe * compute_allegement_anticipe(individu, period, parameters, variable_name, compute_function))
        + (recouvrement_progressif * compute_allegement_progressif(individu, period, parameters, variable_name, compute_function))
        )


def compute_allegement_annuel(individu, period, parameters, variable_name, compute_function):
    if period.start.month < 12:
        return 0
    if period.start.month == 12:
        return sum(
            compute_function(individu, sub_period, parameters)
            for sub_period in period.this_year.get_subperiods(MONTH)
            )


def compute_allegement_anticipe(individu, period, parameters, variable_name, compute_function):
    if period.start.month < 12:
        return compute_function(individu, period.first_month, parameters)
    if period.start.month == 12:
        cumul = individu(
            variable_name,
            Period(('month', period.start.offset('first-of', 'year'), 11)), options = [ADD])
        return sum(
            compute_function(individu, sub_period, parameters)
            for sub_period in period.this_year.get_subperiods(MONTH)
            ) - cumul


def compute_allegement_progressif(individu, period, parameters, variable_name, compute_function):
    if period.start.month == 1:
        return compute_function(individu, period.first_month, parameters)

    if period.start.month > 1:
        up_to_this_month = Period(('month', period.start.offset('first-of', 'year'), period.start.month))
        up_to_previous_month = Period(('month', period.start.offset('first-of', 'year'), period.start.month - 1))
        cumul = individu(variable_name, up_to_previous_month, options = [ADD])
        return sum(
            compute_function(individu, sub_period, parameters)
            for sub_period in up_to_this_month.get_subperiods(MONTH)
            ) - cumul


def taux_exo_cice(assiette_allegement, smic_proratise, cice):
    taux_cice = ((assiette_allegement / (smic_proratise + 1e-16)) <= cice.plafond_smic) * cice.taux
    return taux_cice
