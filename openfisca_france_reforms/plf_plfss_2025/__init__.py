from openfisca_core.reforms import Reform
from openfisca_core.parameters import ParameterNode
from openfisca_core.model_api import *

from openfisca_france.model.base import *

from ..contrefactuel_plf import modify_parameters as modify_parameters_contrefacturel
from ..inflaters import inflate_parameters

from openfisca_france_reforms.plf_plfss_2025.allegement import allegement_general
from openfisca_france_reforms.plf_plfss_2025.cdhr import (
    contribution_differentielle_hauts_revenus_ressources,
    contribution_differentielle_hauts_revenus_decote,
    contribution_differentielle_hauts_revenus_eligible,
    contribution_differentielle_hauts_revenus,
    contribution_exceptionnelle_hauts_revenus_majoration,
    #revenus_nets_apres_impot_menage
    impots_directs
)


import os
reform_date = "2025-01-01"


def modify_parameters(parameters):
    parameters = modify_parameters_contrefacturel(parameters)

    # ------- REVALORISATION IMPOT REVENU -------

    inflateurs = ParameterNode(
        "inflateurs",
        directory_path=os.path.join(os.path.dirname(__file__), "parameters/inflateurs/"),
    )

    parameters.children['inflateurs'].children['inflateur_impot'] = inflateurs.inflateur_impot

    param_ir = parameters.impot_revenu
    liste_param_impot = [
        ## revalo sans texte de loi mais usuelle :
        param_ir.bareme_ir_depuis_1945.bareme,
        param_ir.calcul_impot_revenu.plaf_qf.plafond_avantages_procures_par_demi_part,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_couple,
        param_ir.calcul_impot_revenu.plaf_qf.decote.seuil_celib,
        param_ir.calcul_revenus_imposables.abat_rni.enfant_marie,
        param_ir.calcul_revenus_imposables.charges_deductibles.pensions_alimentaires.plafond,
        ## revalo selon 1e tranche de l'IR
        param_ir.calcul_revenus_imposables.deductions.abatpen,
        param_ir.calcul_revenus_imposables.deductions.abatpro,
        param_ir.calcul_revenus_imposables.charges_deductibles.accueil_personne_agee.plafond,
        param_ir.calcul_revenus_imposables.abat_rni.contribuable_age_invalide,
    ]
    for noeud in liste_param_impot:
        inflate_parameters(
            noeud,
            inflator=parameters(reform_date).inflateurs.inflateur_impot,
            base_year=2023,
            last_year=2024,
            start_instant=f"{2024}-01-01",
            round_ndigits=0,
        )
    # # En attendant les véritables PLF et PLFSS 2025,
    # # - applique une augmentation de 1 € de chaque seuil du barème de l'IR
    # # - applique une augmentation de 1 % du taux de la décote

    # impot_revenu = parameters.impot_revenu
    # bareme = impot_revenu.bareme_ir_depuis_1945.bareme
    # for bracket in bareme.brackets:
    #     threshold = bracket.threshold
    #     threshold_value = threshold.get_at_instant(reform_date)
    #     if threshold_value is not None:
    #         threshold.update(start=reform_date, value=round(threshold_value * 1.048, 2))

    # plaf_qf = impot_revenu.calcul_impot_revenu.plaf_qf
    # decote = plaf_qf.decote

    # ------- REFORME ALLEGEMENT GENERAUX COTISATIONS PATRONALES -------

    # --- ALLEGEMENT GENERAL ---
    # reforme allegement general : taux maximum diminué de deux points (entreprises +50 salariés)
    parameters.prelevements_sociaux.reductions_cotisations_sociales.allegement_general.ensemble_des_entreprises.entreprises_de_50_salaries_et_plus.update(
        start=reform_date, value=0.3034 #valeur_initiale=0.3234 (-2pts de %)
    )
    # reforme allegement general : taux maximum diminué de deux points (entreprises -50 salariés)
    parameters.prelevements_sociaux.reductions_cotisations_sociales.allegement_general.ensemble_des_entreprises.entreprises_de_moins_de_50_salaries.update(
        start=reform_date, value=0.2994 #valeur_initiale=0.3194 (-2pts de %)
    )

    # --- BANDEAU MALADIE ---
    # reforme seuil bandeau maladie : 2.2 au lieu de 2.5
    parameters.prelevements_sociaux.reductions_cotisations_sociales.alleg_gen.mmid.plafond_smic_2023_12_31.update(
        start=reform_date, value=2.2 # valeur_initiale=2.5
    )

    # --- BANDEAU FAMILLE ---
    # reforme seuil bandeau famille : 3.2 au lieu de 3.5
    parameters.prelevements_sociaux.reductions_cotisations_sociales.allegement_cotisation_allocations_familiales.plafond_smic_2023_12_31.update(
        start=reform_date, value=3.2 # valeur_initiale=3.5
    )

    # ------- REFORME ALLEGEMENT COTISATIONS TO-DE - Article 4 PLFSS 2025 -------

    # reforme du plafond d'exonération intégrale : rémunération max passant de 1,2 smic à 1,25 smic
    parameters.prelevements_sociaux.reductions_cotisations_sociales.agricole.tode.plafond_exoneration_integrale.update(
        start=reform_date, value=1.25 #valeur_initiale=1.2
    )

    # Création de la contribution différentielle sur les hauts revenus (CDHR)
    new_node = "contribution_differentielle_hauts_revenus"
    new_node_path = os.path.join(
            os.path.dirname(__file__), 
            f"parameters/impot_revenu/contributions_exceptionnelles/{new_node}"
        )
    cdhr_parameters = ParameterNode(
        new_node,
        directory_path = new_node_path,
    )    
    parameters.impot_revenu.contributions_exceptionnelles.add_child(new_node, cdhr_parameters)

    return parameters


# Cette classe n'hérite pas de ContrefactuelPlf, car actuellement, les méthodes d'une
# réforme ne permettent pas l'héritage entre 2 réformes (La méthode modify_parameters
# n'est pas composable).
# Il faut donc reproduire ici le comportement de la méthode ContrefactuelPlf.apply.
class PlfPlfss2025(Reform):
    name = "PLF & PLFSS 2025"
    tax_benefit_system_name = "openfisca_france_with_indirect_taxation"

    def apply(self):
        self.modify_parameters(modifier_function=modify_parameters)
        self.update_variable(allegement_general)
        
        # Contribution différentielle sur les hauts revenus
        self.add_variable(contribution_differentielle_hauts_revenus_ressources)
        self.add_variable(contribution_differentielle_hauts_revenus_eligible)
        self.add_variable(contribution_differentielle_hauts_revenus)
        self.add_variable(contribution_differentielle_hauts_revenus_decote)
        self.add_variable(contribution_exceptionnelle_hauts_revenus_majoration)
        #self.update_variable(revenus_nets_apres_impot_menage)
        self.update_variable(impots_directs)
