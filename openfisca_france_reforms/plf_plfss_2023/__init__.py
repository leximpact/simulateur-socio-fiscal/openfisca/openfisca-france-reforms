# La réforme proposée par le Projet de loi de finances 2023

import os

from openfisca_core.parameters import load_parameter_file, Parameter
from openfisca_core.periods import period
from openfisca_core.reforms import Reform

from .paje import paje_cmg


reform_date = "2023-01-01"


def modify_parameters(parameters):
    parameters_dir = os.path.join(os.path.dirname(__file__), "parameters")
    for yaml_split_path in walk_dir(parameters_dir, []):
        split_name = yaml_split_path.copy()
        id = split_name[-1].replace(".yaml", "")
        split_name[-1] = id
        parameter = load_parameter_file(
            os.path.join(parameters_dir, *yaml_split_path),
            name=".".join(split_name),
        )
        parent = parameters
        for parent_id in split_name[:-1]:
            child = getattr(parent, parent_id, None)
            assert (
                child is not None
            ), f'Parameter "{parent.name}" has no "{parent_id}" child'
            parent = child
        parent.children[id] = parameter
        setattr(parent, id, parameter)

    # Revalorisation ASF
    revalorisation = 50 / 100
    parameters.prestations_sociales.prestations_familiales.education_presence_parentale.asf.montant_asf.orphelin_assimile_deux_parents.update(
        start=reform_date, value=0.375 * (1 + revalorisation)
    )
    parameters.prestations_sociales.prestations_familiales.education_presence_parentale.asf.montant_asf.orphelin_assimile_seul_parent.update(
        start=reform_date, value=0.2813 * (1 + revalorisation)
    )

    # Nouveau parametre : Extension CMG
    parameters.prestations_sociales.prestations_familiales.petite_enfance.paje.paje_cmg.limite_age.reduite.update(
        start=reform_date, value=None
    )
    parameters.prestations_sociales.prestations_familiales.petite_enfance.paje.paje_cmg.limite_age.pleine.update(
        start=reform_date, value=6
    )
    # Method add_child below fails because paje 2023 has now been added to openfisca-france.
    # parameters.prestations_sociales.prestations_familiales.petite_enfance.paje.paje_cmg.limite_age.add_child(
    #     "etendue",
    #     Parameter(
    #         "etendue",
    #         data=dict(
    #             description="Limite d'âge (stricte) pour une prestation étendue du Complément de libre choix de mode de garde (CMG) de la Prestation d'accueil du jeune enfant (PAJE)",
    #             values={
    #                 reform_date: 12,
    #             },
    #         ),
    #     ),
    # )
    parameters.prestations_sociales.prestations_familiales.petite_enfance.paje.paje_cmg.limite_age.etendue.update(
        start=reform_date, value=12
    )

    return parameters


def walk_dir(root_dir, relative_split_dir):
    dir = os.path.join(root_dir, *relative_split_dir)
    for entry in os.scandir(dir):
        if entry.name.startswith("."):
            continue
        relative_split_path = relative_split_dir + [entry.name]
        if entry.is_dir():
            yield from walk_dir(root_dir, relative_split_path)
        else:
            if not entry.name.endswith(".yaml"):
                continue
            yield relative_split_path


class PlfPlfss2023(Reform):
    name = "PLF & PLFSS 2023"
    tax_benefit_system_name = "openfisca_france_with_indirect_taxation"

    def apply(self):
        self.modify_parameters(modifier_function=modify_parameters)
        self.update_variable(paje_cmg)
