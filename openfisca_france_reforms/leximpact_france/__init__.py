import os
from openfisca_core.reforms import Reform
from openfisca_core.parameters import ParameterNode
from openfisca_france_reforms.leximpact_france.baremisation_csg_crds import (
    csg_deductible_salaire,
    csg_imposable_salaire,
    crds_salaire,
    csg_imposable_non_salarie,
    csg_deductible_non_salarie,
    crds_non_salarie,
    csg_deductible_chomage,
    csg_imposable_chomage,
    crds_chomage,
    csg_deductible_retraite,
    csg_imposable_retraite,
    crds_retraite,
    csg_glo_assimile_salaire_ir_et_ps,
    crds_glo_assimile_salaire_ir_et_ps,
    crds_revenus_capital,
    crds_ppa,
    crds_af,
    crds_ars,
    crds_asf,
    crds_cf,
    crds_paje,
    crds_logement,
    )
from openfisca_france_reforms.leximpact_france.refacto_super_brut_to_disponible import (
    autres_impositions_forfaitaires,
    cotisations_allegement_general,
    cotisations_employeur_assurance_chomage,
    cotisations_employeur_autres,
    cotisations_employeur_retraite_complementaire,
    cotisations_employeur_securite_sociale,
    cotisations_salariales_contributives,
    csg,
    csg_chomage,
    csg_non_salarie,
    csg_retraite,
    csg_salaire,
    irpp_economique,
    minima_sociaux,
    pensions_rentes_complementaires,
    prestations_sociales,
    primes,
    remuneration_brute,
    revenu_disponible,
    revenus_du_capital_avant_prelevements,
    revenus_nets_apres_impot_menage,
    revenus_nets_menage,
    salaire_super_brut,
    vieillesse_employeur,
    vieillesse_salarie
    )

def modify_parameters(parameters):
    deciles_niveau_vie = ParameterNode(
        "deciles_niveau_de_vie",
        directory_path=os.path.join(os.path.dirname(__file__), "parameters/deciles_niveau_de_vie"),
    )
    parameters.add_child("deciles_niveau_de_vie", deciles_niveau_vie)


class LexImpactTaxBenefitSystem(Reform):
    name = "PLF & PLFSS 2025"
    tax_benefit_system_name = "openfisca_france_with_indirect_taxation"

    def apply(self):
        self.modify_parameters(modifier_function=modify_parameters)
    
        for variable in [
            csg_deductible_salaire,
            csg_imposable_salaire,
            crds_salaire,
            csg_imposable_non_salarie,
            csg_deductible_non_salarie,
            crds_non_salarie,
            csg_deductible_chomage,
            csg_imposable_chomage,
            crds_chomage,
            csg_deductible_retraite,
            csg_imposable_retraite,
            crds_retraite,
            csg_glo_assimile_salaire_ir_et_ps,
            crds_glo_assimile_salaire_ir_et_ps,
            crds_revenus_capital,
            crds_ppa,
            crds_af,
            crds_ars,
            crds_asf,
            crds_cf,
            crds_paje,
            crds_logement,
            autres_impositions_forfaitaires,
            cotisations_allegement_general,
            cotisations_employeur_assurance_chomage,
            cotisations_employeur_autres,
            cotisations_employeur_retraite_complementaire,
            cotisations_employeur_securite_sociale,
            cotisations_salariales_contributives,
            csg,
            csg_chomage,
            csg_non_salarie,
            csg_retraite,
            csg_salaire,
            irpp_economique,
            minima_sociaux,
            pensions_rentes_complementaires,
            prestations_sociales,
            primes,
            remuneration_brute,
            revenu_disponible,
            revenus_du_capital_avant_prelevements,
            revenus_nets_apres_impot_menage,
            revenus_nets_menage,
            salaire_super_brut,
            vieillesse_employeur,
            vieillesse_salarie
            ]:
            self.replace_variable(variable)