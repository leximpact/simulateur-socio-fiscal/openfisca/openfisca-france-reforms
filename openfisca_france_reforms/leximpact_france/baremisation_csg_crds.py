import logging

from openfisca_france.model.base import *
from openfisca_france.model.prelevements_obligatoires.prelevements_sociaux.contributions_sociales.remplacement import TypesTauxCSGRetraite, TypesTauxCSGRemplacement

log = logging.getLogger(__name__)

# Dans ce fichier modification des variables de csg et de crds pour permettre la création d'un barème dans l'ui
# Variables modifiees :
## csg_deductible_salaire
## csg_imposable_salaire
## crds_salaire
## csg_imposable_non_salarie
## csg_deductible_non_salarie
## crds_non_salarie
## csg_deductible_chomage
## csg_imposable_chomage
## crds_chomage
## csg_deductible_retraite
## csg_imposable_retraite
## crds_retraite
## csg_glo_assimile_salaire_ir_et_ps
## crds_glo_assimile_salaire_ir_et_ps
## crds_revenus_capital
## crds_ppa
## crds_af
## crds_ars
## crds_asf
## crds_cf
## crds_paje
## crds_logement


def montant_csg_crds_bareme(base_avec_abattement = None, base_sans_abattement = None, indicatrice_taux_plein = None,
        indicatrice_taux_intermediaire = None, indicatrice_taux_reduit = None, abattement_parameter = None, law_node = None, plafond_securite_sociale = None):
    assert law_node is not None
    if base_sans_abattement is None:
        base_sans_abattement = 0
    if base_avec_abattement is None:
        base = base_sans_abattement
    else:
        assert plafond_securite_sociale is not None
        assert abattement_parameter is not None
        base = base_avec_abattement - abattement_parameter.calc(
            base_avec_abattement,
            factor = plafond_securite_sociale,
            round_base_decimals = 2,
            ) + base_sans_abattement
    if indicatrice_taux_plein is None and indicatrice_taux_reduit is None:
        return -law_node.taux.calc(base)
    elif indicatrice_taux_plein is not None and indicatrice_taux_reduit is not None and indicatrice_taux_intermediaire is None:
        return -(
            law_node.taux_plein.calc(base) * indicatrice_taux_plein
            + law_node.taux_reduit.calc(base) * indicatrice_taux_reduit
            )
    else:
        assert indicatrice_taux_plein is not None
        assert indicatrice_taux_reduit is not None
        assert indicatrice_taux_intermediaire is not None
        return -(
            law_node.taux_plein.calc(base) * indicatrice_taux_plein
            + law_node.taux_reduit.calc(base) * indicatrice_taux_reduit
            + law_node.taux_median.calc(base) * indicatrice_taux_intermediaire
            )


class csg_deductible_salaire(Variable):
    calculate_output = calculate_output_add
    value_type = float
    label = 'CSG déductible sur les salaires'
    reference = 'https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006073189/LEGISCTA000006173055/#LEGIARTI000042340733'
    entity = Individu
    definition_period = MONTH
    set_input = set_input_divide_by_period

    def formula(individu, period, parameters):
        assiette_csg_abattue = individu('assiette_csg_abattue', period)
        assiette_csg_non_abattue = individu('assiette_csg_non_abattue', period)
        plafond_securite_sociale = individu('plafond_securite_sociale', period)

        csg = parameters(period).prelevements_sociaux.contributions_sociales.csg
        montant_csg = montant_csg_crds_bareme(
            base_avec_abattement = assiette_csg_abattue,
            base_sans_abattement = assiette_csg_non_abattue,
            abattement_parameter = csg.activite.abattement,
            law_node = csg.activite.deductible,
            plafond_securite_sociale = plafond_securite_sociale,
            )
        return montant_csg


class csg_imposable_salaire(Variable):
    calculate_output = calculate_output_add
    value_type = float
    label = 'CSG imposables sur les salaires'
    entity = Individu
    definition_period = MONTH
    set_input = set_input_divide_by_period

    def formula(individu, period, parameters):
        assiette_csg_abattue = individu('assiette_csg_abattue', period)
        assiette_csg_non_abattue = individu('assiette_csg_non_abattue', period)
        plafond_securite_sociale = individu('plafond_securite_sociale', period)
        csg_parameters = parameters(period).prelevements_sociaux.contributions_sociales.csg

        montant_csg = montant_csg_crds_bareme(
            base_avec_abattement = assiette_csg_abattue,
            base_sans_abattement = assiette_csg_non_abattue,
            abattement_parameter = csg_parameters.activite.abattement,
            law_node = csg_parameters.activite.imposable,
            plafond_securite_sociale = plafond_securite_sociale,
            )

        return montant_csg


class crds_salaire(Variable):
    calculate_output = calculate_output_add
    value_type = float
    label = 'CRDS sur les salaires'
    entity = Individu
    definition_period = MONTH
    set_input = set_input_divide_by_period

    def formula(individu, period, parameters):
        assiette_csg_abattue = individu('assiette_csg_abattue', period)
        assiette_csg_non_abattue = individu('assiette_csg_non_abattue', period)
        plafond_securite_sociale = individu('plafond_securite_sociale', period)

        parameters = parameters(period).prelevements_sociaux.contributions_sociales

        montant_crds = montant_csg_crds_bareme(
            law_node = parameters.crds,
            base_avec_abattement = assiette_csg_abattue,
            base_sans_abattement = assiette_csg_non_abattue,
            abattement_parameter = parameters.csg.activite.abattement,
            plafond_securite_sociale = plafond_securite_sociale,
            )

        return montant_crds


class csg_imposable_non_salarie(Variable):
    value_type = float
    entity = Individu
    label = 'CSG des personnes non salariées'
    definition_period = YEAR

    def formula(individu, period, parameters):
        assiette_csg_crds_non_salarie = individu('assiette_csg_crds_non_salarie', period)
        csg = parameters(period).prelevements_sociaux.contributions_sociales.csg.activite

        montant_csg = montant_csg_crds_bareme(
            base_sans_abattement = assiette_csg_crds_non_salarie,
            law_node = csg.imposable,
            )

        return montant_csg


class csg_deductible_non_salarie(Variable):
    value_type = float
    entity = Individu
    label = 'CSG des personnes non salariées'
    definition_period = YEAR

    def formula(individu, period, parameters):
        assiette_csg_crds_non_salarie = individu('assiette_csg_crds_non_salarie', period)
        csg = parameters(period).prelevements_sociaux.contributions_sociales.csg.activite

        montant_csg = montant_csg_crds_bareme(
            base_sans_abattement = assiette_csg_crds_non_salarie,
            law_node = csg.deductible,
            )

        return montant_csg


class crds_non_salarie(Variable):
    value_type = float
    entity = Individu
    label = 'CRDS des personnes non salariées'
    definition_period = YEAR

    def formula(individu, period, parameters):
        assiette_csg_crds_non_salarie = individu('assiette_csg_crds_non_salarie', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = assiette_csg_crds_non_salarie,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class csg_deductible_chomage(Variable):
    calculate_output = calculate_output_add
    value_type = float
    entity = Individu
    label = 'CSG déductible sur les allocations chômage'
    reference = 'http://vosdroits.service-public.fr/particuliers/F2329.xhtml'
    definition_period = MONTH
    set_input = set_input_divide_by_period
    # TODO : formule à partir de 2015 seulement. Pour les années d'avant, certaines seuils de RFR sont manquants, ainsi que des informations relatives à des exonérations passées.

    def formula_2015(individu, period, parameters):
        csg_imposable_chomage = individu('csg_imposable_chomage', period)
        parameters = parameters(period)
        rfr = individu.foyer_fiscal('rfr', period = period.n_2)
        nbptr = individu.foyer_fiscal('nbptr', period = period.n_2)
        seuils = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils
        seuil_exoneration = seuils.seuil_rfr1.seuil_rfr1 + (nbptr - 1) * 2 * seuils.seuil_rfr1.demi_part_suppl_rfr1
        seuil_reduction = seuils.seuil_rfr2.seuil_rfr2 + (nbptr - 1) * 2 * seuils.seuil_rfr2.demi_part_suppl_rfr2

        taux_csg_remplacement = where(
            rfr <= seuil_exoneration,
            TypesTauxCSGRemplacement.exonere,
            where(
                rfr <= seuil_reduction,
                TypesTauxCSGRemplacement.taux_reduit,
                TypesTauxCSGRemplacement.taux_plein,
                )
            )

        chomage_brut = individu('chomage_brut', period)
        chomage_cotisation_retraite_complementaire = individu('chomage_cotisation_retraite_complementaire', period)
        assiette_csg_chomage = chomage_brut - chomage_cotisation_retraite_complementaire

        montant_csg = montant_csg_crds_bareme(
            base_avec_abattement = assiette_csg_chomage,
            indicatrice_taux_plein = (taux_csg_remplacement == TypesTauxCSGRemplacement.taux_plein),
            indicatrice_taux_reduit = (taux_csg_remplacement == TypesTauxCSGRemplacement.taux_reduit),
            abattement_parameter = parameters.prelevements_sociaux.contributions_sociales.csg.activite.abattement,
            law_node = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.allocations_chomage.deductible,
            plafond_securite_sociale = parameters.prelevements_sociaux.pss.plafond_securite_sociale_mensuel,
            )
        nbh_travail = parameters.marche_travail.salaire_minimum.smic.nb_heures_travail_mensuel

        cho_seuil_exo = (
            parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.allocations_chomage.min_exo
            * nbh_travail
            * parameters.marche_travail.salaire_minimum.smic.smic_b_horaire
            )

        # Approximation annuelle
        salaire_net = individu('salaire_net', period)
        rpns_imposables = individu('rpns_imposables', period, options = [DIVIDE])
        csg_imposable_non_salarie = individu('csg_imposable_non_salarie', period, options = [DIVIDE])
        crds_non_salarie = individu('crds_non_salarie', period, options = [DIVIDE])
        remuneration_activite_nette = salaire_net + rpns_imposables + csg_imposable_non_salarie + crds_non_salarie

        csg_deductible_chomage = max_(
            - montant_csg
            - max_(
                cho_seuil_exo - (remuneration_activite_nette + assiette_csg_chomage + csg_imposable_chomage + montant_csg), 0
                ), 0
            )

        return - csg_deductible_chomage


class csg_imposable_chomage(Variable):
    calculate_output = calculate_output_add
    value_type = float
    entity = Individu
    label = 'CSG imposable sur les allocations chômage'
    reference = 'http://vosdroits.service-public.fr/particuliers/F2329.xhtml'
    definition_period = MONTH
    set_input = set_input_divide_by_period
    # TODO : formule à partir de 2015 seulement. Pour les années d'avant, certaines seuils de RFR sont manquants, ainsi que des informations relatives à des exonérations passées.

    def formula_2015(individu, period, parameters):
        parameters = parameters(period)

        rfr = individu.foyer_fiscal('rfr', period = period.n_2)
        nbptr = individu.foyer_fiscal('nbptr', period = period.n_2)
        seuils = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils
        seuil_exoneration = seuils.seuil_rfr1.seuil_rfr1 + (nbptr - 1) * 2 * seuils.seuil_rfr1.demi_part_suppl_rfr1
        seuil_reduction = seuils.seuil_rfr2.seuil_rfr2 + (nbptr - 1) * 2 * seuils.seuil_rfr2.demi_part_suppl_rfr2

        taux_csg_remplacement = where(
            rfr <= seuil_exoneration,
            TypesTauxCSGRemplacement.exonere,
            where(
                rfr <= seuil_reduction,
                TypesTauxCSGRemplacement.taux_reduit,
                TypesTauxCSGRemplacement.taux_plein,
                )
            )

        chomage_brut = individu('chomage_brut', period)
        chomage_cotisation_retraite_complementaire = individu('chomage_cotisation_retraite_complementaire', period)
        assiette_csg_chomage = chomage_brut - chomage_cotisation_retraite_complementaire

        montant_csg = montant_csg_crds_bareme(
            base_avec_abattement = assiette_csg_chomage,
            indicatrice_taux_plein = (taux_csg_remplacement == TypesTauxCSGRemplacement.taux_plein),
            indicatrice_taux_reduit = (taux_csg_remplacement == TypesTauxCSGRemplacement.taux_reduit),
            abattement_parameter = parameters.prelevements_sociaux.contributions_sociales.csg.activite.abattement,
            law_node = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.allocations_chomage.imposable,
            plafond_securite_sociale = parameters.prelevements_sociaux.pss.plafond_securite_sociale_mensuel,
            )
        nbh_travail = parameters.marche_travail.salaire_minimum.smic.nb_heures_travail_mensuel
        cho_seuil_exo = (
            parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.allocations_chomage.min_exo
            * nbh_travail
            * parameters.marche_travail.salaire_minimum.smic.smic_b_horaire
            )

        salaire_net = individu('salaire_net', period)
        # Approximation annuelle
        rpns_imposables = individu('rpns_imposables', period, options = [DIVIDE])
        csg_imposable_non_salarie = individu('csg_imposable_non_salarie', period, options = [DIVIDE])
        crds_non_salarie = individu('crds_non_salarie', period, options = [DIVIDE])
        remuneration_activite_nette = salaire_net + rpns_imposables + csg_imposable_non_salarie + crds_non_salarie

        csg_imposable_chomage = max_(- montant_csg - max_(cho_seuil_exo - (remuneration_activite_nette + assiette_csg_chomage + montant_csg), 0), 0)
        return - csg_imposable_chomage


class crds_chomage(Variable):
    calculate_output = calculate_output_add
    value_type = float
    entity = Individu
    label = 'CRDS sur les allocations chômage'
    reference = 'http://www.insee.fr/fr/methodes/default.asp?page=definitions/contrib-remb-dette-sociale.htm'
    definition_period = MONTH
    set_input = set_input_divide_by_period
    # TODO : formule à partir de 2015 seulement. Pour les années d'avant, certaines seuils de RFR sont manquants, ainsi que des informations relatives à des exonérations passées.

    def formula_2015(individu, period, parameters):
        csg_deductible_chomage = individu('csg_deductible_chomage', period)
        csg_imposable_chomage = individu('csg_imposable_chomage', period)
        parameters = parameters(period)
        rfr = individu.foyer_fiscal('rfr', period = period.n_2)
        nbptr = individu.foyer_fiscal('nbptr', period = period.n_2)
        seuils = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils
        seuil_exoneration = seuils.seuil_rfr1.seuil_rfr1 + (nbptr - 1) * 2 * seuils.seuil_rfr1.demi_part_suppl_rfr1
        seuil_reduction = seuils.seuil_rfr2.seuil_rfr2 + (nbptr - 1) * 2 * seuils.seuil_rfr2.demi_part_suppl_rfr2

        taux_csg_remplacement = where(
            rfr <= seuil_exoneration,
            TypesTauxCSGRemplacement.exonere,
            where(
                rfr <= seuil_reduction,
                TypesTauxCSGRemplacement.taux_reduit,
                TypesTauxCSGRemplacement.taux_plein,
                )
            )
        smic_h_b = parameters.marche_travail.salaire_minimum.smic.smic_b_horaire
        # salaire_mensuel_reference = chomage_brut / .7
        # heures_mensuelles = min_(salaire_mensuel_reference / smic_h_b, 35 * 52 / 12)
        heures_mensuelles = parameters.marche_travail.salaire_minimum.smic.nb_heures_travail_mensuel

        chomage_brut = individu('chomage_brut', period)
        chomage_cotisation_retraite_complementaire = individu('chomage_cotisation_retraite_complementaire', period)
        assiette_crds_chomage = chomage_brut - chomage_cotisation_retraite_complementaire

        cho_seuil_exo = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.allocations_chomage.min_exo * heures_mensuelles * smic_h_b
        eligible = (
            (taux_csg_remplacement == TypesTauxCSGRemplacement.taux_reduit)
            + (taux_csg_remplacement == TypesTauxCSGRemplacement.taux_plein)
            )
        montant_crds = montant_csg_crds_bareme(
            base_avec_abattement = assiette_crds_chomage,
            abattement_parameter = parameters.prelevements_sociaux.contributions_sociales.csg.activite.abattement,
            law_node = parameters.prelevements_sociaux.contributions_sociales.crds,
            plafond_securite_sociale = parameters.prelevements_sociaux.pss.plafond_securite_sociale_mensuel,
            ) * eligible

        salaire_net = individu('salaire_net', period)
        # Approximation annuelle
        rpns_imposables = individu('rpns_imposables', period, options = [DIVIDE])
        csg_imposable_non_salarie = individu('csg_imposable_non_salarie', period, options = [DIVIDE])
        crds_non_salarie = individu('crds_non_salarie', period, options = [DIVIDE])
        remuneration_activite_nette = salaire_net + rpns_imposables + csg_imposable_non_salarie + crds_non_salarie

        crds_chomage = max_(
            - montant_crds - max_(
                cho_seuil_exo - (remuneration_activite_nette + assiette_crds_chomage + csg_imposable_chomage + csg_deductible_chomage + montant_crds), 0
                ), 0
            )
        return - crds_chomage


class csg_deductible_retraite(Variable):
    calculate_output = calculate_output_add
    value_type = float
    entity = Individu
    label = 'CSG déductible sur les pensions de retraite'
    reference = 'https://www.lassuranceretraite.fr/cs/Satellite/PUBPrincipale/Retraites/Paiement-Votre-Retraite/Prelevements-Sociaux?packedargs=null'
    definition_period = MONTH
    set_input = set_input_divide_by_period
    # TODO : formule à partir de 2015 seulement. Pour les années d'avant, certaines seuils de RFR sont manquants, ainsi que des informations relatives à des exonérations passées.

    def formula_2019(individu, period, parameters):
        retraite_brute = individu('retraite_brute', period)
        rfr = individu.foyer_fiscal('rfr', period = period.n_2)
        nbptr = individu.foyer_fiscal('nbptr', period = period.n_2)
        parameters = parameters(period)
        seuils = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils
        seuil_exoneration = seuils.seuil_rfr1.seuil_rfr1 + (nbptr - 1) * 2 * seuils.seuil_rfr1.demi_part_suppl_rfr1
        seuil_reduction = seuils.seuil_rfr2.seuil_rfr2 + (nbptr - 1) * 2 * seuils.seuil_rfr2.demi_part_suppl_rfr2
        seuil_taux_intermediaire = seuils.seuil_rfr3.seuil_rfr3 + (nbptr - 1) * 2 * seuils.seuil_rfr3.demi_part_suppl_rfr3

        taux_csg_retraite = select(
            [rfr <= seuil_exoneration, rfr <= seuil_reduction, rfr <= seuil_taux_intermediaire, rfr > seuil_taux_intermediaire],
            [TypesTauxCSGRetraite.exonere, TypesTauxCSGRetraite.taux_reduit, TypesTauxCSGRetraite.taux_intermediaire, TypesTauxCSGRetraite.taux_plein]
            )

        montant_csg = montant_csg_crds_bareme(
            base_sans_abattement = retraite_brute,
            indicatrice_taux_plein = (taux_csg_retraite == TypesTauxCSGRetraite.taux_plein),
            indicatrice_taux_reduit = (taux_csg_retraite == TypesTauxCSGRetraite.taux_reduit),
            indicatrice_taux_intermediaire = (taux_csg_retraite == TypesTauxCSGRetraite.taux_intermediaire),
            law_node = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.deductible,
            plafond_securite_sociale = parameters.prelevements_sociaux.pss.plafond_securite_sociale_mensuel,
            )
        return montant_csg


class csg_imposable_retraite(Variable):
    calculate_output = calculate_output_add
    value_type = float
    entity = Individu
    label = 'CSG imposable sur les pensions de retraite'
    reference = 'https://www.lassuranceretraite.fr/cs/Satellite/PUBPrincipale/Retraites/Paiement-Votre-Retraite/Prelevements-Sociaux?packedargs=null'
    definition_period = MONTH
    set_input = set_input_divide_by_period
    # TODO : formule à partir de 2015 seulement. Pour les années d'avant, certaines seuils de RFR sont manquants, ainsi que des informations relatives à des exonérations passées.

    def formula_2019(individu, period, parameters):
        retraite_brute = individu('retraite_brute', period)
        rfr = individu.foyer_fiscal('rfr', period = period.n_2)
        nbptr = individu.foyer_fiscal('nbptr', period = period.n_2)
        parameters = parameters(period)
        seuils = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils
        seuil_exoneration = seuils.seuil_rfr1.seuil_rfr1 + (nbptr - 1) * 2 * seuils.seuil_rfr1.demi_part_suppl_rfr1
        seuil_reduction = seuils.seuil_rfr2.seuil_rfr2 + (nbptr - 1) * 2 * seuils.seuil_rfr2.demi_part_suppl_rfr2
        seuil_taux_intermediaire = seuils.seuil_rfr3.seuil_rfr3 + (nbptr - 1) * 2 * seuils.seuil_rfr3.demi_part_suppl_rfr3

        taux_csg_retraite = select(
            [rfr <= seuil_exoneration, rfr <= seuil_reduction, rfr <= seuil_taux_intermediaire, rfr > seuil_taux_intermediaire],
            [TypesTauxCSGRetraite.exonere, TypesTauxCSGRetraite.taux_reduit, TypesTauxCSGRetraite.taux_intermediaire, TypesTauxCSGRetraite.taux_plein]
            )

        montant_csg = montant_csg_crds_bareme(
            base_sans_abattement = retraite_brute,
            indicatrice_taux_plein = (taux_csg_retraite == TypesTauxCSGRetraite.taux_plein),
            indicatrice_taux_reduit = (taux_csg_retraite == TypesTauxCSGRetraite.taux_reduit),
            indicatrice_taux_intermediaire = (taux_csg_retraite == TypesTauxCSGRetraite.taux_intermediaire),
            law_node = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.pensions_retraite_invalidite.imposable,
            )
        return montant_csg


class crds_retraite(Variable):
    calculate_output = calculate_output_add
    value_type = float
    entity = Individu
    label = 'CRDS sur les pensions de retraite'
    reference = 'http://www.pensions.bercy.gouv.fr/vous-%C3%AAtes-retrait%C3%A9-ou-pensionn%C3%A9/le-calcul-de-ma-pension/les-pr%C3%A9l%C3%A8vements-effectu%C3%A9s-sur-ma-pension'
    definition_period = MONTH
    set_input = set_input_divide_by_period
    # TODO : formule à partir de 2015 seulement. Pour les années d'avant, certaines seuils de RFR sont manquants, ainsi que des informations relatives à des exonérations passées.

    def formula_2019(individu, period, parameters):
        retraite_brute = individu('retraite_brute', period)
        rfr = individu.foyer_fiscal('rfr', period = period.n_2)
        nbptr = individu.foyer_fiscal('nbptr', period = period.n_2)
        parameters = parameters(period)
        seuils = parameters.prelevements_sociaux.contributions_sociales.csg.remplacement.seuils
        seuil_exoneration = seuils.seuil_rfr1.seuil_rfr1 + (nbptr - 1) * 2 * seuils.seuil_rfr1.demi_part_suppl_rfr1
        seuil_reduction = seuils.seuil_rfr2.seuil_rfr2 + (nbptr - 1) * 2 * seuils.seuil_rfr2.demi_part_suppl_rfr2
        seuil_taux_intermediaire = seuils.seuil_rfr3.seuil_rfr3 + (nbptr - 1) * 2 * seuils.seuil_rfr3.demi_part_suppl_rfr3

        taux_csg_retraite = select(
            [rfr <= seuil_exoneration, rfr <= seuil_reduction, rfr <= seuil_taux_intermediaire, rfr > seuil_taux_intermediaire],
            [TypesTauxCSGRetraite.exonere, TypesTauxCSGRetraite.taux_reduit, TypesTauxCSGRetraite.taux_intermediaire, TypesTauxCSGRetraite.taux_plein]
            )

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = retraite_brute,
            law_node = parameters.prelevements_sociaux.contributions_sociales.crds,
            plafond_securite_sociale = parameters.prelevements_sociaux.pss.plafond_securite_sociale_mensuel,
            ) * (taux_csg_retraite != TypesTauxCSGRetraite.exonere)

        return montant_crds


class csg_glo_assimile_salaire_ir_et_ps(Variable):
    calculate_output = calculate_output_add
    value_type = float
    label = "CSG sur GLO assimilés salaires pour les prélèvements sociaux, en plus de l'être pour l'IR (cases 1TT et similaires)"
    entity = Individu
    definition_period = YEAR
    set_input = set_input_divide_by_period

    def formula(individu, period, parameters):
        f1tt = individu('f1tt', period)
        csg_activite = parameters(period).prelevements_sociaux.contributions_sociales.csg.activite
        taux = csg_activite.imposable.taux.rates[0] + csg_activite.deductible.taux.rates[0]
        return - f1tt * taux


class crds_glo_assimile_salaire_ir_et_ps(Variable):
    calculate_output = calculate_output_add
    value_type = float
    label = "CRDS sur GLO assimilés salaires pour les prélèvements sociaux, en plus de l'être pour l'IR (cases 1TT et similaires)"
    entity = Individu
    definition_period = YEAR
    set_input = set_input_divide_by_period

    def formula(individu, period, parameters):
        f1tt = individu('f1tt', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = f1tt,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class crds_revenus_capital(Variable):
    value_type = float
    entity = FoyerFiscal
    label = 'CRDS sur les revenus du capital'
    definition_period = YEAR

    def formula(foyer_fiscal, period, parameters):
        assiette_csg_revenus_capital = foyer_fiscal('assiette_csg_revenus_capital', period)
        crds_glo_assimile_salaire_ir_et_ps_i = foyer_fiscal.members('crds_glo_assimile_salaire_ir_et_ps', period)
        crds_glo_assimile_salaire_ir_et_ps = foyer_fiscal.sum(crds_glo_assimile_salaire_ir_et_ps_i)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = assiette_csg_revenus_capital,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return (montant_crds + crds_glo_assimile_salaire_ir_et_ps)

class crds_ppa(Variable):
    value_type = float
    entity = Famille
    label = "CRDS sur la prime pour l'activité"
    definition_period = MONTH
    set_input = set_input_divide_by_period

    def formula_2016_01_01(famille, period, parameters):
        ppa = famille('ppa', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = ppa,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class crds_af(Variable):
    value_type = float
    entity = Famille
    label = 'CRDS sur les allocations familiales'
    definition_period = MONTH

    def formula(famille, period, parameters):
        af = famille('af', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = af,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class crds_ars(Variable):
    value_type = float
    entity = Famille
    label = "CRDS sur l'allocation de rentrée scolaire"
    reference = 'http://www.cleiss.fr/docs/regimes/regime_francea1.html'
    definition_period = YEAR

    def formula(famille, period, parameters):
        ars = famille('ars', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = ars,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class crds_asf(Variable):
    value_type = float
    entity = Famille
    label = "CRDS sur l'allocation de soutien familial'"
    definition_period = MONTH

    def formula(famille, period, parameters):
        asf = famille('asf', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = asf,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class crds_cf(Variable):
    value_type = float
    entity = Famille
    label = 'CRDS sur le complément familial'
    definition_period = MONTH

    def formula(famille, period, parameters):
        cf = famille('cf', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = cf,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class crds_paje(Variable):
    value_type = float
    entity = Famille
    label = 'CRDS sur la PAJE - Ensemble des prestations'
    definition_period = MONTH

    def formula(famille, period, parameters):
        paje = famille('paje', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = paje,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds


class crds_logement(Variable):
    calculate_output = calculate_output_add
    value_type = float
    entity = Famille
    label = 'CRDS des allocations logement'
    reference = 'http://vosdroits.service-public.fr/particuliers/F17585.xhtml'
    definition_period = MONTH
    set_input = set_input_divide_by_period

    def formula(famille, period, parameters):
        aide_logement_montant_brut = famille('aide_logement_montant_brut_crds', period)

        law = parameters(period)

        montant_crds = montant_csg_crds_bareme(
            base_sans_abattement = aide_logement_montant_brut,
            law_node = law.prelevements_sociaux.contributions_sociales.crds,
            )

        return montant_crds