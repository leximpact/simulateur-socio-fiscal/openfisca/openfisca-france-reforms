# Openfisca-France-Reforms

_Some reforms for French OpenFisca tax-benefit system_

By:

- [Sandra Chakroun](mailto:sandra.chakroun@assemblee-nationale.fr)
- [Benoît Courty](mailto:benoit.courty@assemblee-nationale.fr),
- [Chloé Lallemand](mailto:chloe.lallemand@assemblee-nationale.fr)
- [Dorine Lambinet](mailto:dorine.lambinet@assemblee-nationale.fr)
- [Emmanuel Raviart](mailto:emmanuel.raviart@assemblee-nationale.fr)

Copyright (C) 2021, 2022, 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/simulateur-socio-fiscal/openfisca/openfisca-france-reforms/

> Openfisca-France-Reforms is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Openfisca-France-Reforms is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
