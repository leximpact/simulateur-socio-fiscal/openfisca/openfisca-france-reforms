// import toml from "@ltd/j-toml"
import assert from "assert"
import { $, fetch, /* fs */ } from "zx"

// interface Package {
//   name: string
//   version: string
// }

// interface PyProject {
//   tool: {
//     poetry: {
//       dependencies: { [name: string]: unknown }
//       name: string
//       version: string
//     }
//   }
// }

export interface VersionObject {
  major: number
  minor: number
  patch: number
}

const {
  CI_COMMIT_BRANCH,
  CI_COMMIT_TAG,
  CI_COMMIT_TITLE,
  CI_DEFAULT_BRANCH,
  CI_JOB_TOKEN,
  CI_PIPELINE_SOURCE,
  CI_SERVER_URL,
  // SSH_KNOWN_HOSTS,
  // SSH_PRIVATE_KEY,
} = process.env
// const packagePath = "package.json"
// const poetryLockPath = "poetry.lock"
// const pyProjectTomlPath = "pyproject.toml"

// function checkVersionObject(
//   {
//     major: majorReference,
//     minor: minorReference,
//     patch: patchReference,
//   }: VersionObject,
//   versionObject: VersionObject | undefined,
// ): boolean {
//   if (versionObject === undefined) {
//     return true
//   }
//   const { major, minor, patch } = versionObject
//   if (major < majorReference) {
//     return true
//   }
//   if (major === majorReference) {
//     if (minor < minorReference) {
//       return true
//     }
//     if (minor === minorReference) {
//       return patch <= patchReference || patch === patchReference + 1
//     }
//     return minor === minorReference + 1 && patch === 0
//   }
//   return major === majorReference + 1 && minor === 0 && patch === 0
// }

// async function commitAndPushWithUpdatedVersions(
//   nextVersionObject?: VersionObject | undefined,
// ) {
//   // Retrieve current OpenFisca-France version.
//   const poetryLockToml = await fs.readFile(poetryLockPath)
//   const poetryLock = toml.parse(poetryLockToml, "\n")
//   const openFiscaFrancePackage = (poetryLock.package as Package[]).find(
//     (pkg) => pkg.name === "openfisca-france",
//   )
//   const openFiscaFranceVersion = openFiscaFrancePackage!.version

//   if (nextVersionObject === undefined) {
//     // Retrieve next version of OpenFisca-France-Reforms.
//     nextVersionObject = await latestVersionObjectFromTags()
//     assert.notStrictEqual(nextVersionObject, undefined)
//     nextVersionObject!.patch++
//   }
//   const nextVersion = versionFromObject(nextVersionObject!)

//   // Update pyproject.toml with next version of OpenFisca-France-Reforms…
//   let pyProjectToml = await fs.readFile(pyProjectTomlPath, "utf-8")
//   pyProjectToml = pyProjectToml.replace(
//     /^version = "(.*?)"$/m,
//     `version = "${nextVersion}"`,
//   )
//   // … and latest version of OpenFisca-France.
//   pyProjectToml = pyProjectToml.replace(
//     /^openfisca-france = ".*"$/im,
//     `openfisca-france = ">= ${openFiscaFranceVersion}"`,
//   )
//   await fs.writeFile(pyProjectTomlPath, pyProjectToml, "utf-8")

//   // Commit, add a version tag & push.
//   await $`git add .`
//   if ((await $`git diff --quiet --staged`.exitCode) !== 0) {
//     await $`git commit -m "${nextVersion} (openfisca-france@${openFiscaFranceVersion})"`
//     await $`git push --set-upstream`
//   }
//   await $`git tag -a "${nextVersion}" -m "${nextVersion} (openfisca-france@${openFiscaFranceVersion})"`
//   await $`git push --set-upstream --tags`
// }

// async function configureGit() {
//   console.log("Configuring git…")

//   // Set the Git user name and email.
//   await $`git config --global user.email "admin+openfisca-france-reforms-ci@tax-benefit.org"`
//   await $`git config --global user.name "OpenFisca-France Reforms CI"`

//   console.log("Git configuration completed.")
// }

// async function configureSsh() {
//   console.log("Configuring ssh…")

//   // Note: `eval $(ssh-agent -s)` must be done before calling this script because it
//   // creates 2 environment variables (then used by ssh clients).
//   // // Install ssh-agent if not already installed, to be able to use git with ssh.
//   // if ((await $`which ssh-agent`.exitCode) !== 0) {
//   //   await $`apt install -y openssh-client`
//   // }
//   // // Run ssh-agent (inside the build environment)
//   // await $`eval $(ssh-agent -s)`

//   // Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
//   // Use `tr` to fix line endings which makes ed25519 keys work without
//   // extra base64 encoding.
//   // https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
//   if (SSH_PRIVATE_KEY !== undefined) {
//     await $`echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -`
//   }

//   if (SSH_KNOWN_HOSTS !== undefined) {
//     // Create the SSH directory and give it the right permissions
//     await $`mkdir -p ~/.ssh`
//     await $`chmod 700 ~/.ssh`
//     // Accept the SSH host keys of GitLab server.
//     await $`echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts`
//     await $`chmod 644 ~/.ssh/known_hosts`
//   }

//   console.log("Ssh configuration completed.")
// }

// async function latestVersionObjectFromTags(): Promise<
//   VersionObject | undefined
// > {
//   // Retrieve all tags.
//   await $`git fetch --all --tags`

//   return (await $`git tag --list`).stdout
//     .split("\n")
//     .map(objectFromVersion)
//     .filter((versionObject) => versionObject !== undefined)
//     .sort((versionObject1, versionObject2) =>
//       versionObject1!.major !== versionObject2!.major
//         ? versionObject2!.major - versionObject1!.major
//         : versionObject1!.minor !== versionObject2!.minor
//         ? versionObject2!.minor - versionObject1!.minor
//         : versionObject2!.patch - versionObject1!.patch,
//     )[0]
// }

async function main() {
  console.log("Starting gitlab-ci.ts…")

  // await configureGit()
  // await configureSsh()

  console.log(`Handling "${CI_PIPELINE_SOURCE}" event…`)
  switch (CI_PIPELINE_SOURCE) {
    case "api":
    case "schedule":
    case "trigger":
    case "web": {
      if (CI_COMMIT_BRANCH === CI_DEFAULT_BRANCH) {
        // A pipeline has been triggered (for example when OpenFisca-France has been updated or a user as
        // launched a new pipeline manually or…).
        // => A new version of OpenFisca-France-Reforms must be created if and only if a dependency has
        // changed.

        console.log(`Handling trigger…`)

        // await resetGitRepository()

        // const pyProjectToml = await fs.readFile(pyProjectTomlPath)
        // const pyProject = toml.parse(
        //   pyProjectToml,
        //   "\n",
        // ) as unknown as PyProject
        // const { poetry } = pyProject.tool

        await $`poetry update`
        await $`poetry install`

        await $`git add .`
        if ((await $`git diff --quiet --staged`.exitCode) !== 0) {
          // Dependencies of openfisca-france-reforms have changed.

          // Ensure that everything works.
          // await $`poetry run python -m openfisca_france_reforms.plf_2022.scripts.run_test`
          // await $`poetry run python -m openfisca_france_reforms.prime_partage_valeur.scripts.run_test`

          //  => Don't generate a new version of openfisca-france-reforms,
          // because it is done by Tax-Benefit CI.
          // const nextVersionObject = await nextVersionObjectFromPoetryAndTags(
          //   poetry,
          // )
          // await commitAndPushWithUpdatedVersions(nextVersionObject)

          await triggerLexImpactOpenFiscaJsonModelPipeline()
          await triggerLexImpactSocioFiscalApiPipeline()
        }
      } else {
        console.log(
          `Unhandled event "${CI_PIPELINE_SOURCE}" in branch "${CI_COMMIT_BRANCH}".`,
        )
      }
      break
    }
    // case "merge_request_event": {
    //   console.log(
    //     `Merge request from branch ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME} to branch ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}`,
    //   )

    //   // Test OpenFisca-France-Reforms with its current dependencies.
    //   await $`poetry install`
    //   await $`poetry run python -m openfisca_france_reforms.plf_2022.scripts.run_test`
    //   await $`poetry run python -m openfisca_france_reforms.prime_partage_valeur.scripts.run_test`

    //   // Test OpenFisca-France-Reforms with the latest dependencies (especially OpenFisca-France).
    //   await $`poetry update`
    //   await $`poetry install`
    //   await $`poetry run python -m openfisca_france_reforms.plf_2022.scripts.run_test`
    //   await $`poetry run python -m openfisca_france_reforms.prime_partage_valeur.scripts.run_test`

    //   break
    // }
    case "push": {
      if (CI_COMMIT_BRANCH !== undefined) {
        console.log(`Push to branch ${CI_COMMIT_BRANCH}`)

        if (CI_COMMIT_BRANCH.match(/^\d+_\d+_\d+$/) != null) {
          console.log(
            `Ignoring commit in version branch (for branch ${CI_COMMIT_BRANCH}).`,
          )
        } else if (CI_COMMIT_TITLE?.match(/^\d+\.\d+\.\d+( |$)/) != null) {
          console.log(
            `Ignoring version commit (for version ${CI_COMMIT_TITLE}).`,
          )
        } else {
          // await resetGitRepository()

          // const pyProjectToml = await fs.readFile(pyProjectTomlPath)
          // const pyProject = toml.parse(
          //   pyProjectToml,
          //   "\n",
          // ) as unknown as PyProject
          // const { poetry } = pyProject.tool

          // const nextVersionObject = await nextVersionObjectFromPoetryAndTags(
          //   poetry,
          // )

          // Test OpenFisca-France-Reforms with its current dependencies, to ensure that
          // the committer didn't make mistakes when pushing.
          await $`poetry install`
          // await $`poetry run python -m openfisca_france_reforms.plf_2022.scripts.run_test`
          // await $`poetry run python -m openfisca_france_reforms.prime_partage_valeur.scripts.run_test`

          // Now, test OpenFisca-France-Reforms with its latest dependencies (especially OpenFisca-France).
          await $`poetry update`
          await $`poetry install`
          // await $`poetry run python -m openfisca_france_reforms.plf_2022.scripts.run_test`
          // await $`poetry run python -m openfisca_france_reforms.prime_partage_valeur.scripts.run_test`

          if (CI_COMMIT_BRANCH === CI_DEFAULT_BRANCH) {
            // A merge request has been merged into master (ie content of OpenFisca-France-Reforms has been changed).

            //  => Don't generate a new version of openfisca-france-reforms,
            // because it is done by Tax-Benefit CI.
            // await commitAndPushWithUpdatedVersions(nextVersionObject)

            await triggerLexImpactOpenFiscaJsonModelPipeline()
            await triggerLexImpactSocioFiscalApiPipeline()
          }
        }
      } else if (CI_COMMIT_TAG !== undefined) {
        console.log(`Pushing commit tag "${CI_COMMIT_TAG}"…`)
      } else if (CI_COMMIT_TAG !== undefined) {
        console.log(`Unhandled push event.`)
      }
      break
    }
    default:
      console.log(
        `Unhandled event "${CI_PIPELINE_SOURCE}" in branch "${CI_COMMIT_BRANCH}".`,
      )
  }
}

// function maxVersionObject(
//   versionObject1: VersionObject,
//   versionObject2: VersionObject | undefined,
// ): VersionObject {
//   if (versionObject2 === undefined) {
//     return versionObject1
//   }
//   const { major: major1, minor: minor1, patch: patch1 } = versionObject1
//   const { major: major2, minor: minor2, patch: patch2 } = versionObject2
//   if (major1 < major2) {
//     return versionObject2
//   }
//   if (major1 > major2) {
//     return versionObject1
//   }
//   if (minor1 < minor2) {
//     return versionObject2
//   }
//   if (minor1 > minor2) {
//     return versionObject1
//   }
//   if (patch1 < patch2) {
//     return versionObject2
//   }
//   if (patch1 > patch2) {
//     return versionObject1
//   }
//   // Both versions are the same. Return one of them.
//   return versionObject1
// }

// async function nextVersionObjectFromPoetryAndTags(
//   poetry: PyProject["tool"]["poetry"],
// ): Promise<VersionObject> {
//   // Retrieve current version of project.
//   const tagVersionObject = (await latestVersionObjectFromTags()) ?? {
//     major: 0,
//     minor: 0,
//     patch: 0,
//   }
//   const tagVersion = versionFromObject(tagVersionObject)
//   let nextVersionObject = {
//     ...tagVersionObject,
//     patch: tagVersionObject!.patch + 1,
//   }

//   // Ensure that the version numbers of project packages are
//   // compatible with last version tag.
//   const { version } = poetry
//   const versionObject = objectFromVersion(version)
//   assert(
//     checkVersionObject(tagVersionObject, versionObject),
//     `In ${pyProjectTomlPath}, project version should be compatible with ${tagVersion}, got "${version}" instead.`,
//   )
//   nextVersionObject = maxVersionObject(nextVersionObject, versionObject)
//   const pkg = await fs.readJson(packagePath)
//   const { version: packageVersion } = pkg
//   const packageVersionObject = objectFromVersion(packageVersion)
//   assert(
//     checkVersionObject(tagVersionObject, packageVersionObject),
//     `In ${packagePath}, project version should be compatible with ${tagVersion}, got "${packageVersion}" instead.`,
//   )
//   nextVersionObject = maxVersionObject(nextVersionObject, packageVersionObject)

//   return nextVersionObject
// }

// function objectFromVersion(
//   version: string | undefined,
// ): VersionObject | undefined {
//   if (version === undefined) {
//     return undefined
//   }
//   const match = version.match(/^(\d+)\.(\d+)\.(\d+)$/) as string[]
//   if (match === null) {
//     return undefined
//   }
//   return {
//     major: parseInt(match[1]),
//     minor: parseInt(match[2]),
//     patch: parseInt(match[3]),
//   }
// }

// async function resetGitRepository() {
//   // Reset current repo, because it may have been tranformed by a previous CI that failed.
//   await $`git switch ${CI_COMMIT_BRANCH}`
//   await $`git fetch origin ${CI_COMMIT_BRANCH}`
//   await $`git reset --hard origin/${CI_COMMIT_BRANCH}`
//   await $`git status`
//   if (CI_COMMIT_BRANCH === CI_DEFAULT_BRANCH) {
//     await $`git remote set-url origin git@git.leximpact.dev:leximpact/openfisca-france-reforms.git`
//   } else {
//     await $`git remote set-url origin https://git.leximpact.dev/leximpact/openfisca-france-reforms.git`
//   }
// }

async function triggerLexImpactOpenFiscaJsonModelPipeline() {
  const url = new URL(
    `/api/v4/projects/16/trigger/pipeline`,
    CI_SERVER_URL,
  ).toString()
  console.log(
    "Triggering LexImpact OpenFisca JSON Model pipeline on master branch…",
  )
  const response = await fetch(url, {
    body: new URLSearchParams({
      ref: "master",
      token: CI_JOB_TOKEN!,
      "variables[PUBLISH_JSON]": "true",
    }).toString(),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    method: "POST",
  })
  assert(
    response.ok,
    `Unexpected response from ${url}: ${response.status} ${
      response.statusText
    }\n${await response.text()}`,
  )
}

async function triggerLexImpactSocioFiscalApiPipeline() {
  const url = new URL(
    `/api/v4/projects/3/trigger/pipeline`,
    CI_SERVER_URL,
  ).toString()
  console.log("Triggering LexImpact socio-fiscal API pipeline on master branch…")
  const response = await fetch(url, {
    body: new URLSearchParams({
      ref: "master",
      token: CI_JOB_TOKEN!,
    }).toString(),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    method: "POST",
  })
  assert(
    response.ok,
    `Unexpected response from ${url}: ${response.status} ${
      response.statusText
    }\n${await response.text()}`,
  )
}

// function versionFromObject({ major, minor, patch }: VersionObject): string {
//   return `${major}.${minor}.${patch}`
// }

main()
  .then(() => {
    process.exit(0)
  })
  .catch((error: unknown) => {
    console.error(error)
    process.exit(1)
  })
